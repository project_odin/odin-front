;(function() {

    'use strict';

        function applicationController ($scope, $rootScope, $window, Sidebar) {

            $scope.Sidebar = Sidebar;

            $rootScope.$on('loggedIn', function() {
                $rootScope.isLoggedIn = true;
                $rootScope.user_type = $window.localStorage.user_type;
                $rootScope.user      = $window.localStorage.user;
            });

            $rootScope.$on('loggedOut', function() {
                $rootScope.isLoggedIn = false;
                $window.localStorage.removeItem("user");
                $window.localStorage.removeItem("satellizer_token");
                $window.localStorage.removeItem("user_type");
            });
        };

        function httpInterceptor ($q, $location, $window, $rootScope) {
            return {
                responseError : function(response) {
                    
                    var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid', 'invalid_credentials', 'cannot_create_token'];
 
                    angular.forEach(rejectionReasons, function (value, key) {
                        if (response.data.error === value) {
                            localStorage.removeItem('user');
                            localStorage.removeItem('satellizer_token');
                        }
                    });

                    return $q.reject(response);

                }
            }
        };
        
        function run($rootScope, $window, $route, $location) {
                            
            $rootScope.config = JSON.parse($window.localStorage.ngConf);

            $window.localStorage.removeItem('ngConf');

            $rootScope.$on('$routeChangeStart', function() {
                $rootScope.previousPage = $rootScope.currentPage || '/';
                $rootScope.currentPage  = $location.$$path;
            });
        };

        function config($routeProvider, $httpProvider, $ocLazyLoadProvider) {

            
            $routeProvider

                .when('/500', {
                    templateUrl : 'errorpages/500.html'
                })

                .when('/503', {
                    templateUrl : 'errorpages/503.html'
                })

                .when('/unauthorize', {
                    templateUrl : 'errorpages/unauthorize.html'
                })

                .when('/', {
                    templateUrl  : 'src/modules/components/candidate/index/indexView.html',
                });

            $httpProvider.interceptors.push('httpInterceptor');


            $ocLazyLoadProvider.config({
                loadedModules : ['application']
            });

        };
            
        angular
            .module('application', [
                'ngRoute', 
                'ngResource', 
                'ngAnimate',
                'ngFileUpload',
                'google.places',
                'lumx', 
                'oc.lazyLoad', 
                'satellizer', 
                'smart-table',
                'module.login',
                'module.shared',
                'module.admin',
                'module.hr',
                'module.candidate',
                'module.employee'
            ])
            .controller('applicationController', ['$scope', '$rootScope', '$window', 'Sidebar', applicationController])
            .config(['$routeProvider', '$httpProvider', '$ocLazyLoadProvider', config])
            .factory('httpInterceptor', ['$q', '$location', '$window', '$rootScope', httpInterceptor])
            .run(['$rootScope', '$window', '$route', '$location', run]);

            angular.element(document).ready(function() {

                $.getJSON('/config/config.json', 
                    function(config) {
                        localStorage.ngConf = JSON.stringify(config);
                        angular.bootstrap(document, ['application']);
                    })
                    .error(function() {
                        console.log('Configuration File is missing.');
                    }
                )
            });

})();
