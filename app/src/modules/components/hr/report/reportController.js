;(function() {

    'use strict';

    angular
        .module('module.hr')
        .controller('ReportController', [
            '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   '$timeout'
        ,   'Upload'
        ,   'Filters'
        ,   'LxNotificationService'
        ,   'LxDialogService'
        ,   '$scope'
        ,   '$filter'
        ,   ReportController]);

        function ReportController(
            $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   $timeout
        ,   Upload
        ,   Filters
        ,   LxNotificationService
        ,   LxDialogService
        ,   $scope
        ,   $filter
        ) {

            var vm = this;
            vm.displayTable = true;

            vm.toggleDisplay = function(table) {
                vm.display = '';

                $timeout(function() {
                    vm.display = table; 
                }, 500);
            }

            Filters.getFilters({'filters[]' : ['positions', 'departments']}).then(function(response) {
                vm.positions      = response.filters.positions;
                vm.departments    = response.filters.departments;
                vm.employee_types = [{'id' : 1, 'type' : 'Regular'}, {'id' : 2, 'type' : 'Contractual'}]
            });

            vm.employeeFilter     = {};
            vm.resignationsFilter = {};
            vm.terminationsFilter = {};
            vm.leaveFilter        = {};
            vm.openingFilter      = {};
            vm.candidatesFilter   = {};
            vm.payrollFilter      = {};
            vm.announcementFilter = {};

            vm.generateReport = function(reportType) {
                    
                switch(reportType) {
                    case 'employee'     : 
                        generateEmployeeReport(vm.employeeFilter);
                        break; 
                    case 'resignations' : 
                        generateEesignationsReport(vm.resignationsFilter);
                        break; 
                    case 'terminations' : 
                        generateTerminationsReport(vm.terminationsFilter);
                        break; 
                    case 'leave'        : 
                        generateLeaveReport(vm.leaveFilter);
                        break; 
                    case 'opening'      : 
                        generateOpeningReport(vm.openingFilter);
                        break; 
                    case 'candidates'   : 
                        generateCandidatesReport(vm.candidatesFilter);
                        break; 
                    case 'payroll'      : 
                        generatePayrollReport(vm.payrollFilter);
                        break; 
                    case 'announcement' : 
                        generateAnnouncementReport(vm.announcementFilter);
                        break; 
                }
            }

            function generateEmployeeReport(filters) {
                var url =  'http://api.apservices.com/api/report/';

                var department = (typeof filters.department === 'undefined') ? '' : filters.department.id;
                var employee_type  = (typeof filters.employee_type === 'undefined') ? '' : filters.employee_type.id;
                var position       = (typeof filters.position === 'undefined') ? '' : filters.position.id;
                var date_hired     = (typeof filters.date_hired === 'undefined') ? '' : $filter('date')(filters.date_hired, 'yyyy-MM-dd');


                url +=  '?department='    + department;
                url +=  '&employee_type=' + employee_type;
                url +=  '&position='      + position;
                url +=  '&date_hired='    + date_hired;
                url +=  '&report_type='   + 'employee';
                
                $window.open(url, '_blank');
            }

            function generateEesignationsReport(filters) {

            }

            function generateTerminationsReport(filters) {

            }

            function generateLeaveReport(filters) {

            }

            function generateOpeningReport(filters) {
            }

            function generateCandidatesReport(filters) {
                $window.open('http://api.apservices.com/api/report?report_type=candidate', '_blank');     
            }

            function generatePayrollReport(filters) {

                var url =  'http://api.apservices.com/api/report?report_type=payroll';

                var date_from = (typeof filters.date_from === 'undefined') ? '' : $filter('date')(filters.date_from, 'yyyy-MM-dd');
                var date_to   = (typeof filters.date_to === 'undefined') ? '' : $filter('date')(filters.date_to, 'yyyy-MM-dd');

                url +=  '&date_from=' + date_from;
                url +=  '&date_to=' + date_to;
                
                $window.open(url, '_blank');
            }

            function generateAnnouncementReport(filters) {

            }


        }

})();