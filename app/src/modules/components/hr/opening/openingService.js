;(function() {

    'use strict';

    angular
        .module('module.hr')
        .service('OpeningService', ['$resource', '$rootScope', OpeningService]);

        function OpeningService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {
                        createOpening : {
                            url : $rootScope.config.application_server + 'api/hr/opening',
                            method: 'POST',
                        },

                        getOpenings : {
                            url : $rootScope.config.application_server + 'api/hr/opening',
                            method: 'GET',
                        },

                        publishJobPost : {
                            url : $rootScope.config.application_server + 'api/hr/publish/opening/:id',
                            params : {id : '@id'},
                            method: 'GET'
                        }
                    }
                );


                var OpeningService = {

                    openings : null,

                    createOpening : function (params) {
                        return api.createOpening(params).$promise
                    },

                    getOpenings : function () {
                        
                        var self = this;

                        return api.getOpenings().$promise.then( function(response) {
                            self.openings = response.openings;
                        })
                    },

                    publishJobPost : function(id) {
                        return api.publishJobPost({'id' : id}).$promise
                    }
                };

                return OpeningService;
        }

})();