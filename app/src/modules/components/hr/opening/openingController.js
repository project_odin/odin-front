;(function() {

    'use strict';

    angular
        .module('module.hr')
        .controller('OpeningController', 
            [
                '$auth', '$http', '$location', '$window', '$rootScope', 'Filters',
                'Sidebar', 'OpeningService', 'LxDialogService', 'LxNotificationService', OpeningController
            ]
        );

        function OpeningController(
            $auth, $http, $location, $window, $rootScope, Filters,
            Sidebar, OpeningService, LxDialogService, LxNotificationService
        ) {

            var vm = this;

            vm.filters = [];
            
            Filters.getFilters({'filters[]' : ['companies']}).then(function(response) {
                vm.filters.companies = response.filters.companies;
            });
            
            vm.Sidebar = Sidebar;
            vm.locations      = [[{id : 'ft', type : 'Full Time'}, {id : 'pt', type : 'Part Time'}]]; 
            vm.openingTypes   = [{id : 'ft', type : 'Full Time'}, {id : 'pt', type : 'Part Time'}];
                
            vm.openings       = OpeningService.openings;

            vm.showAddOpening = showAddOpening;
            vm.createOpening  = createOpening;
            vm.deleteOpening  = deleteOpening;
            vm.opening        = {};
            vm.viewList       = true;
            vm.publishJobPost = function (id) {
                console.log(1)
                LxNotificationService
                    .confirm('Delete Opening', 'Continue delete opening?', 
                        { 
                            cancel :'No', 
                            ok :'Yes' 
                        }, function(answer) {
                            if (answer) {
                                OpeningService.publishJobPost(id).then(function() {
                                    vm.viewList = false;  
                                    OpeningService.getOpenings().then(function() {
                                        vm.viewList = true;  
                                        OpeningService.openings;
                                    });
                                });

                            }
                        }
                    );
            }

            function deleteOpening(openingId) {
                LxNotificationService
                    .confirm('Delete Opening', 'Continue delete opening?', 
                        { 
                            cancel :'No', 
                            ok :'Yes' 
                        }, function(answer) {
                            if (answer) {
                                OpeningService.deleteOpening(openingId).then(function() {
                                    OpeningService.getOpenings().then(function(){
                                        vm.openings = OpeningService.openings;  
                                        LxNotificationService.info('Opening deleted'); 
                                    });
                                });

                            }
                        }
                    );
            }


            vm.updateOpening = function() {
                OpeningService.updateOpening(vm.opening).then(function(){

                })
            } 

            function createOpening() {

                vm.loading = true;

                vm.opening.type = vm.selected_opening_type.id;
                vm.opening.company_id = vm.selected_company.id;
                vm.opening.location = vm.selected_company.location; 

                OpeningService.createOpening(vm.opening)
                    .then(function(response) {

                        vm.loading = false;
                        LxDialogService.close('addOpeningDialog');

                        OpeningService.getOpenings().then(function() {
                            vm.openings = OpeningService.openings;
                        });

                        vm.opening = {};
                        vm.selected_location = null;
                        vm.selected_location = null;

                        $location.path('/hr/opening')

                    }, function(error) {

                        var errors = error.data
                        angular.forEach(errors, function(value, key) {
                            LxNotificationService.error(value);
                        });

                        vm.loading = false;

                    });

            }

            function showAddOpening() {
                LxDialogService.open('addOpeningDialog');
            }

            vm.locations = {
                list: [],
                update: function(newFilter) {
                    if (newFilter) {
                        vm.locations.loading = true;
                        var request = { input : escape(newFilter) };
                        var autocompleteService = new google.maps.places.AutocompleteService();
                        autocompleteService.getPlacePredictions(request, function (predictions, status) {
                            vm.locations.list = predictions;
                        });

                        if (vm.locations.list.length > 0){
                            vm.locations.loading = false;
                        }
                    }
                    else {
                        vm.locations.list = false;
                    }
                },
                toModel: function(data, callback) {
                    if (data) {
                        callback(data.description);
                    } else {
                        callback();
                    }
                },
                toSelection: function(data, callback) {
                    if (data) {

                        var request = { input : escape(data) };
                        var autocompleteService = new google.maps.places.AutocompleteService();

                        autocompleteService.getPlacePredictions(request, function (predictions, status) {
                            callback(predictions);

                        });
                    } else {
                        callback();
                    }
                },
                loading: false
            };
        }

})();