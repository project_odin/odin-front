;(function() {

	'use strict';

	angular
		.module('module.hr')
		.controller('CandidateController', [
			'$auth'
		, 	'$http'
		, 	'$location'
		, 	'$window'
		, 	'$rootScope'
		, 	'Sidebar'
		,	'CandidatesService'
		,	'LxDialogService'
		, 	'LxNotificationService'
		, 	CandidateController
		]);

		function CandidateController(
			$auth
		, 	$http
		, 	$location
		, 	$window
		, 	$rootScope
		, 	Sidebar
		, 	CandidatesService
		,	LxDialogService
		,	LxNotificationService
		) {

			var vm = this;
			
			vm.Sidebar = Sidebar;

			vm.candidates = null;

			vm.candidates = CandidatesService.candidates;
			countPerCategory();

			vm.viewApplicationDetails = viewApplicationDetails;
			vm.moveToStep = moveToStep;
			vm.hireApplicant = hireApplicant;
			vm.candidate = {};

			function countPerCategory() {

				vm.counts = {
					screening : 0,
					phone 	  : 0,
					face 	  : 0,
					job	      : 0
				}


				angular.forEach(vm.candidates, function(value, key) {
					switch(value.status) {
						case 's': vm.counts.screening++; break; 
						case 'p': vm.counts.phone++; break; 
						case 'f': vm.counts.face++; break; 
						case 'j': vm.counts.job++; break; 
					}

				});
			}

			function viewApplicationDetails (applicationId, status) {
				CandidatesService.getCandidate(applicationId).then(function() {
					vm.candidate = CandidatesService.candidate;
                	LxDialogService.open('viewApplicationDetails');
				});
			}
			
			vm.displayTable = true;
			
			function hireApplicant (id, opening) {
				var params = {
					status 		: 'j',
					opening_id  : opening
				}

				var applicantId = id;

				 LxNotificationService
				 	.confirm(
				 		'Hire Applicant', 
				 		'You are going to hire this applicant, continuing this action will move this applicant to Employee List. Do you want to continue?', 
				 		{ cancel:'No', ok:'Yes' }, 
				 		function(answer)
        				{
            				if (answer) {
            					vm.displayTable = false;
								CandidatesService.moveToStep(params, applicantId).then(function(response) {

									CandidatesService.getCandidates().then(function() {
										vm.displayTable = true;
										vm.candidates = CandidatesService.candidates;
										countPerCategory();
										LxNotificationService.success('Applicant Hired.');
									});

								});
            				}
        				});

			}

			function moveToStep () {

				var params = {
					opening_id  : vm.candidate[0].opening_id,
					status 		: vm.candidate[0].application_status
				}
				var applicantId = vm.candidate[0].candidate_id;


				 LxNotificationService
				 	.confirm(
				 		'Move to another step', 
				 		'You are about to move an applicant to another step of application. Do you want to continue?', 
				 		{ cancel:'Disagree', ok:'Agree' }, 
				 		function(answer)
        				{
            				if (answer) {
								CandidatesService.moveToStep(params, applicantId).then(function(response) {

									if (response.action === 'hire') {
										$rootScope.newEmployee = response.data;
										console.log($rootScope.newEmployee)
										$location.path = '/hr/employee';	
									}

									CandidatesService.getCandidates().then(function() {
										LxNotificationService.success('Applicant moved.');
										LxDialogService.close('viewApplicationDetails');
										vm.candidates = CandidatesService.candidates;
										countPerCategory();
									});
								});
            				}
        				});
			}
		}

})();