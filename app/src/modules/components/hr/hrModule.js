;(function() {

    'use strict';

    angular
        .module('module.hr', ['ngRoute', 'satellizer', 'module.shared', 'smart-table'])
        .config(['$routeProvider', '$authProvider', function($routeProvider, $authProvider) {

            var authenticate = ['AuthenticationService', function(AuthenticationService) {
                return AuthenticationService.checkAuth();
            }];

           $routeProvider
                .when('/hr/dashboard', {
                    templateUrl     : 'src/modules/components/hr/dashboard/dashboardView.html',
                    controller      : 'DashboardController',
                    controllerAs    : 'vm',
                    module          : 'hr',
                    resolve         : {
                        DashboardController: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.hr",
                                    files: [
                                        "src/modules/components/hr/dashboard/dashboardController.js",
                                        "src/modules/components/hr/dashboard/dashboardService.js",
                                        "src/modules/components/hr/candidate/candidateService.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth   : authenticate
                    }
                })

                .when('/hr/employee', {
                    templateUrl     : 'src/modules/components/hr/employee/employeeView.html',
                    controller      : 'EmployeeController',
                    controllerAs    : 'vm',
                    module          : 'hr',
                    resolve         : {
                        EmployeeController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.hr",
                                    files: [
                                        "src/modules/components/hr/employee/employeeController.js",
                                        "src/modules/components/hr/employee/employeeService.js",
                                        "src/modules/components/hr/leave/leaveController.js",
                                        "src/modules/components/hr/leave/leaveService.js",
                                        "src/modules/components/hr/timesheet/timesheetController.js",
                                        "src/modules/components/hr/timesheet/timesheetService.js"
                                    ]
                                }
                            ]).then(function(){
                                var EmployeeService = $injector.get('EmployeeService');
                                return EmployeeService.getEmployees();
                            }).then(function() {
                                var EmployeeLeaveService = $injector.get('EmployeeLeaveService');
                                return EmployeeLeaveService.getEmployeeLeaveRequests();
                            }).then(function() {
                                var TimesheetService = $injector.get('TimesheetService');
                                return TimesheetService.getTimesheets();
                            });
                        }],
                        checkAuth    : authenticate
                    }
                })

                .when('/hr/recruitment', {
                    templateUrl     : 'src/modules/components/hr/recruitment/recruitmentView.html',
                    controller      : 'RecruitmentController',
                    controllerAs    : 'vm',
                    module          : 'hr',
                    resolve         : {
                        EmployeeController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.hr",
                                    files: [
                                        "src/modules/components/hr/recruitment/recruitmentController.js",
                                        "src/modules/components/hr/recruitment/opening/openingController.js",
                                        "src/modules/components/hr/recruitment/opening/openingService.js",
                                        "src/modules/components/hr/candidate/candidateController.js",
                                        "src/modules/components/hr/candidate/candidateService.js"
                                    ]
                                }
                            ]).then(function(){
                                var OpeningService = $injector.get('OpeningService');
                                return OpeningService.getOpenings();
                            }).then(function(){
                                var CandidatesService = $injector.get('CandidatesService');
                                return CandidatesService.getCandidates();
                            });
                        }],
                        checkAuth    : authenticate
                    }
                })
                

                .when('/hr/opening/add', {
                    templateUrl     : 'src/modules/components/hr/opening/openingViewAdd.html',
                    controller      : 'OpeningController',
                    controllerAs    : 'vm',
                    module          : 'hr',
                    resolve         : {
                        OpeningController: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.hr",
                                    files: [
                                        "src/modules/components/hr/opening/openingController.js",
                                        "src/modules/components/hr/opening/openingService.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/hr/payroll', {
                    templateUrl     : 'src/modules/components/hr/payroll/payrollView.html',
                    controller      : 'PayrollController',
                    controllerAs    : 'vm',
                    module          : 'hr',
                    resolve         : {
                        PayrollController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.hr",
                                    files: [
                                        "src/modules/components/hr/payroll/payrollController.js",
                                        "src/modules/components/hr/payroll/payrollService.js"
                                    ]
                                }
                            ]).then(function(){
                                var PayrollService = $injector.get('PayrollService');
                                return PayrollService.getPayrollSummary();
                            });
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/hr/announcement', {
                    templateUrl     : 'src/modules/components/hr/announcement/announcementView.html',
                    controller      : 'AnnouncementController',
                    controllerAs    : 'vm',
                    module          : 'hr',
                    resolve         : {
                        AnnouncementController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.hr",
                                    files: [
                                        "src/modules/components/hr/announcement/announcementController.js",
                                        "src/modules/components/hr/announcement/announcementService.js"
                                    ]
                                }
                            ]).then(function() {
                                var AnnouncementService = $injector.get('AnnouncementService');
                                return AnnouncementService.getAnnouncements();
                            });
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/hr/reports', {
                    templateUrl     : 'src/modules/components/hr/report/reportView.html',
                    controller      : 'ReportController',
                    controllerAs    : 'vm',
                    module          : 'hr',
                    resolve         : {
                        ReportController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.hr",
                                    files: [
                                        "src/modules/components/hr/report/reportController.js",
                                        "src/modules/components/hr/report/reportService.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/hr/account', {
                    templateUrl     : 'src/modules/shared/account/accountView.html',
                    controller      : 'AccountController',
                    controllerAs    : 'vm',
                    module          : 'hr',
                    resolve         : {
                        AccountController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.shared",
                                    files: [
                                        "src/modules/shared/account/accountController.js",
                                        "src/modules/shared/account/accountService.js"
                                    ]
                                }
                            ]).then(function() {
                                var AccountService = $injector.get('AccountService');
                                return AccountService.getUser()

                            });
                        }],
                        checkAuth   : authenticate
                    }
                });

        }
    ]);

})();