;(function() {

    'use strict';

    angular
        .module('module.hr')
        .controller('AnnouncementController', [
            '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   'Sidebar'
        ,   'AnnouncementService'
        ,   'LxDialogService'
        ,   'LxNotificationService'
        ,   AnnouncementController
        ]);

        function AnnouncementController(
            $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   Sidebar
        ,   AnnouncementService
        ,   LxDialogService
        ,   LxNotificationService
        ) {

            var vm = this;

            vm.announcements = AnnouncementService.announcements;
            vm.displayTable = true;
            vm.loading = false;

            vm.addAnnouncement = function() {
                vm.view = 'add';
                LxDialogService.open('addAnnouncementDialog');
            }

            vm.confirmAddAnnouncement = function() {
                var params = {
                    title : "Add Announcement",
                    details: "Continue add announcement?",
                    ok : "Yes",
                    close : "Cancel"
                }

                alertAction(params, function() {

                    AnnouncementService.createAnnouncement(vm.announcement).then(function(){
                        vm.displayTable = false;
                        AnnouncementService.getAnnouncements().then(function() {
                            vm.displayTable = true;
                            LxDialogService.close('addAnnouncementDialog');    
                            LxNotificationService.success('Announcement successfully added');
                            vm.announcements = AnnouncementService.announcements;                             
                        })
                    })

                });
            }

            vm.viewDetails = function(id) {
                vm.view = 'edit';
                AnnouncementService.getAnnouncement(id).then(function() {
                    vm.announcement = AnnouncementService.announcement;
                    LxDialogService.open('addAnnouncementDialog');
                })

            }

            function alertAction(params, callback){
                LxNotificationService
                    .confirm(
                        params.title, 
                        params.details, 
                        { cancel:params.close, ok:params.ok }, 
                        function(answer)
                        {
                            if (answer) {
                                callback();
                            }
                        });
            }

            vm.publishAnnouncement = function(id) {

                var params = {
                    title : "Publish Announcement",
                    details: "Continue publish announcement?",
                    ok : "Yes",
                    close : "Cancel"
                }

                alertAction(params, function() {

                    AnnouncementService.publishAnnouncement(id).then(function(){
                        vm.displayTable = false;
                        AnnouncementService.getAnnouncements().then(function() {
                            LxNotificationService.success('Announcement successfully published');
                            vm.displayTable = true;
                            vm.announcements = AnnouncementService.announcements;                             
                        })
                    })

                });
            }

            vm.unpublishAnnouncement = function(id) {

                var params = {
                    title : "Unpublish Announcement",
                    details: "Continue unpublish announcement?",
                    ok : "Yes",
                    close : "Cancel"
                }

                alertAction(params, function() {

                    AnnouncementService.unpublishAnnouncement(id).then(function(){
                        vm.displayTable = false;
                        AnnouncementService.getAnnouncements().then(function() {
                            vm.displayTable = true;
                            vm.announcements = AnnouncementService.announcements;             
                            LxNotificationService.success('Announcement successfully unpublished');                
                        })
                    })

                });
            }

            vm.confirmEditAnnouncement = function(id) {

                var params = {
                    title : "Edit Announcement",
                    details: "Continue edit announcement?",
                    ok : "Yes",
                    close : "Cancel"
                }

                alertAction(params, function() {

                    AnnouncementService.updateAnnouncement(vm.announcement, id).then(function(){
                        vm.displayTable = false;
                        AnnouncementService.getAnnouncements().then(function() {
                            vm.displayTable = true;
                            vm.announcements = AnnouncementService.announcements;  
                            LxNotificationService.success('Announcement successfully updated');
                            LxDialogService.close('addAnnouncementDialog');                           
                        })
                    })

                });
            }

        }

})();