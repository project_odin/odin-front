;(function() {

    'use strict';

    angular
        .module('module.hr')
        .controller('TimesheetController', [
            '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   'Upload'
        ,   'LxNotificationService'
        ,   'LxDialogService'
        ,   'TimesheetService'
        ,   '$scope'
        ,   LeaveController]);

        function LeaveController(
            $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   Upload
        ,   LxNotificationService
        ,   LxDialogService
        ,   TimesheetService
        ,   $scope
        ) {

            var vm = this;
            vm.displayTable = true;
            vm.timesheet = TimesheetService.timesheet;

            $rootScope.$on('timesheetUploaded', function() {
                vm.displayTable = false;
                 TimesheetService.getTimesheets().then(function() {
                    vm.timesheet = TimesheetService.timesheet; 
                    vm.displayTable = true; 
                 });
            });

        }

})();