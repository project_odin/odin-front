;(function() {

    'use strict';

    angular
        .module('module.hr')
        .service('CandidatesService', ['$resource', '$rootScope', CandidatesService]);

        function CandidatesService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {
                        getCandidates : {
                            url : $rootScope.config.application_server + 'api/hr/candidates',
                            method: 'GET',
                        },

                        getCandidate : {
                            url : $rootScope.config.application_server + 'api/hr/candidates/:id',
                            method: 'GET',
                            params : {id : '@id'}
                        },

                        moveToStep : {
                            url : $rootScope.config.application_server + 'api/hr/candidates/:id',
                            params : {id : '@id'},
                            method : 'PUT'
                        }
                    }
                );

                var CandidatesService = {

                    candidates : null,
                    candidate : null,
                    
                    getCandidates : function () {
                        
                        var self = this;

                        return api.getCandidates().$promise.then( function(response) {
                            self.candidates = response.candidates;
                        })
                    },

                    getCandidate : function(id) {

                        var self = this;

                        return api.getCandidate({id : id}).$promise.then(function(response){
                            self.candidate = response.candidate;
                        })
                    },

                    moveToStep : function (params, id) {

                        params.id = id;

                        return api.moveToStep(params).$promise;
                    }

                };

                return CandidatesService;
        }

})();