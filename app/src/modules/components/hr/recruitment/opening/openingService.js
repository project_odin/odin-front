;(function() {

    'use strict';

    angular
        .module('module.hr')
        .service('OpeningService', ['$resource', '$rootScope', OpeningService]);

        function OpeningService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {
                        createOpening : {
                            url : $rootScope.config.application_server + 'api/hr/opening',
                            method: 'POST',
                        },

                        getOpenings : {
                            url : $rootScope.config.application_server + 'api/hr/opening',
                            method: 'GET',
                        },

                        publishJobPost : {
                            url : $rootScope.config.application_server + 'api/hr/publish/opening/:id',
                            params : {id : '@id'},
                            method: 'GET'
                        },

                        unpublishJobPost : {
                            url : $rootScope.config.application_server + 'api/hr/unpublish/opening/:id',
                            params : {id : '@id'},
                            method: 'GET'
                        },

                        getOpening : {
                          url : $rootScope.config.application_server + 'api/hr/opening/:id',
                          params : {id : '@id'},
                          method : 'GET'
                        },

                        updateOpening : {
                          url : $rootScope.config.application_server + 'api/hr/opening/:id',
                          params : {id : '@id'},
                          method: 'PUT'
                        }
                    }
                );

                var OpeningService = {

                    openings : null,

                    createOpening : function (params) {
                        return api.createOpening(params).$promise
                    },

                    getOpenings : function () {
                        
                        var self = this;

                        return api.getOpenings().$promise.then( function(response) {
                            self.openings = response.openings;
                        })
                    },

                    publishJobPost : function(id) {
                        return api.publishJobPost({'id' : id}).$promise
                    },

                    unpublishJobPost : function(id) {
                        return api.unpublishJobPost({'id' : id}).$promise
                    },

                    getOpening : function(id){
                        var self = this;
                        return api.getOpening({'id' : id}).$promise.then(function(response){                            
                            self.opening = response.opening;
                        });
                    },

                    updateOpening : function(params, id){

                        var data = {
                            id              : params.id,
                            title           : params.title,
                            opening_type    : params.opening_type_id,                     
                            location        : params.location,                         
                            job_description : params.description,                  
                            qualifications  : params.qualifications,                   
                            vacancies       : params.vacancy,                        
                            company_id      : params.company_id                       
                        }

                        return api.updateOpening(data).$promise;

                    },

                };

                return OpeningService;
        }

})();