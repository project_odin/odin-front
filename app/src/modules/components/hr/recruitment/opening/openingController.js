;(function() {

    'use strict';

    angular
        .module('module.hr')
        .controller('OpeningController', 
            [
                '$auth', '$http', '$location', '$window', '$rootScope', 'Filters',
                'Sidebar', 'OpeningService', 'LxDialogService', 'LxNotificationService', OpeningController
            ]
        );

        function OpeningController(
            $auth, $http, $location, $window, $rootScope, Filters,
            Sidebar, OpeningService, LxDialogService, LxNotificationService
        ) {
            var vm = this;
            vm.view = 'add';
            vm.viewList = true;  
            vm.filters = [];
            
            Filters.getFilters({'filters[]' : ['companies']}).then(function(response) {
                vm.filters.companies = response.filters.companies;
            });
            vm.publishJobPost = function (id) {
                LxNotificationService
                    .confirm('Publish Opening', 'Continue publish opening?', 
                        { 
                            cancel :'No', 
                            ok :'Yes' 
                        }, function(answer) {
                            if (answer) {
                                OpeningService.publishJobPost(id).then(function() {
                                    vm.viewList = false;  
                                    OpeningService.getOpenings().then(function() {
                                        vm.viewList = true;  
                                        vm.openings = OpeningService.openings;
                                    });
                                });

                            }
                        }
                    );
            }
            vm.unpublishJobPost = function (id) {
                LxNotificationService
                    .confirm('Unpublish Opening', 'Continue unpublish opening?', 
                        { 
                            cancel :'No', 
                            ok :'Yes' 
                        }, function(answer) {
                            if (answer) {
                                OpeningService.unpublishJobPost(id).then(function() {
                                    vm.viewList = false;  
                                    OpeningService.getOpenings().then(function() {
                                        vm.viewList = true;  
                                        vm.openings = OpeningService.openings;
                                    });
                                });

                            }
                        }
                    );
            }


            vm.Sidebar = Sidebar;
            vm.locations      = [[{id : 'ft', type : 'Full Time'}, {id : 'pt', type : 'Part Time'}]]; 
            vm.openingTypes   = [{id : 'ft', type : 'Full Time'}, {id : 'pt', type : 'Part Time'}];
                
            vm.openings       = OpeningService.openings;

            vm.showAddOpening = showAddOpening;
            vm.createOpening  = createOpening;
            vm.deleteOpening  = deleteOpening;
            vm.opening        = {};

            function deleteOpening(openingId) {
                LxNotificationService
                    .confirm('Delete Opening', 'Continue delete opening?', 
                        { 
                            cancel :'No', 
                            ok :'Yes' 
                        }, function(answer) {
                            if (answer) {
                                OpeningService.deleteOpening(openingId).then(function() {
                                    OpeningService.getOpenings().then(function(){
                                        vm.openings = OpeningService.openings;  
                                        LxNotificationService.info('Opening deleted'); 
                                    });
                                });

                            }
                        }
                    );
            }

            vm.showEditOpening = function(id) {
                $rootScope.view = 'edit';
                OpeningService.getOpening(id).then(function() {
                    vm.opening = OpeningService.opening;
                    vm.opening.description = vm.opening.job_description;
                    vm.opening.vacancy = vm.opening.vacancies;

                    var opening_type_id = (vm.opening.opening_type === 'ft' ? 'ft' : 'pt');
                    var opening_type    = (vm.opening.opening_type === 'ft' ? 'Full Time' : 'Part Time');

                    vm.selected_opening_type = { id : opening_type_id, type : opening_type};
                    vm.selected_company = {id : vm.opening.companies.id, name : vm.opening.companies.name};
                    vm.selected_company.location = vm.opening.companies.location;
                    LxDialogService.open('addOpeningDialog');
                });
            }

            vm.updateOpening = function() {

                vm.opening.opening_type_id = vm.selected_opening_type.id;
                vm.opening.company_id = vm.selected_company.id;
                vm.opening.location = vm.selected_company.location; 

                OpeningService.updateOpening(vm.opening).then(function(){

                })
            } 


            function createOpening() {

                vm.loading = true;

                vm.opening.type = vm.selected_opening_type.id;
                vm.opening.company_id = vm.selected_company.id;
                vm.opening.location = vm.selected_company.location; 

                console.log(vm.opening);

                OpeningService.createOpening(vm.opening)
                    .then(function(response) {
                        vm.viewList = false;  
                        vm.loading = false;
                        LxDialogService.close('addOpeningDialog');

                        OpeningService.getOpenings().then(function() {
                            vm.openings = OpeningService.openings;
                            vm.viewList = true;  
                        });

                        vm.opening = {};
                        vm.selected_location = null;
                        vm.selected_location = null;

                    }, function(error) {

                        var errors = error.data
                        angular.forEach(errors, function(value, key) {
                            LxNotificationService.error(value);
                        });

                        vm.loading = false;

                    });

            }

            function showAddOpening() {
                $rootScope.view = 'add';
                LxDialogService.open('addOpeningDialog');
            }

            vm.locations = {
                list: [],
                update: function(newFilter) {
                    if (newFilter) {
                        vm.locations.loading = true;
                        var request = { input : escape(newFilter) };
                        var autocompleteService = new google.maps.places.AutocompleteService();
                        autocompleteService.getPlacePredictions(request, function (predictions, status) {
                            vm.locations.list = predictions;
                        });

                        if (vm.locations.list.length > 0){
                            vm.locations.loading = false;
                        }
                    }
                    else {
                        vm.locations.list = false;
                    }
                },
                toModel: function(data, callback) {
                    if (data) {
                        callback(data.description);
                    } else {
                        callback();
                    }
                },
                toSelection: function(data, callback) {
                    if (data) {

                        var request = { input : escape(data) };
                        var autocompleteService = new google.maps.places.AutocompleteService();

                        autocompleteService.getPlacePredictions(request, function (predictions, status) {
                            callback(predictions);

                        });
                    } else {
                        callback();
                    }
                },
                loading: false
            };
        }

})();