;(function() {

    'use strict';

    angular
        .module('module.hr')
        .controller('RecruitmentController', [
            '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   '$timeout'
        ,   'Sidebar'
        ,   'LxDialogService'
        ,   'LxNotificationService'
        ,   'Filters'
        ,   RecruitmentController]);

        function RecruitmentController(
            $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   $timeout
        ,   Sidebar
        ,   LxDialogService
        ,   LxNotificationService
        ,   Filters
        ) {

            var vm = this;
            
            vm.Sidebar = Sidebar;
            vm.filters = [];
            vm.display = 'opening';
            vm.view = 'add';

            vm.toggleList = function(table) {
                console.log(table);
                vm.display = '';

                $timeout(function() {
                    vm.display = table; 
                }, 500);
            }

            vm.showAddOpening = function() {
                $rootScope.view = 'add';
                LxDialogService.open('addOpeningDialog');
            }

        }
})();


