;(function() {

	'use strict';

	angular
		.module('module.hr')
		.controller('LeaveController', [
            '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   'Sidebar'
        ,   'LxNotificationService'
        ,   'LxDialogService'
        ,   'EmployeeLeaveService'
        ,   LeaveController]);

		function LeaveController(
            $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   Sidebar
        ,   LxNotificationService
        ,   LxDialogService
        ,   EmployeeLeaveService
        ) {

			var vm = this;
			vm.Sidebar = Sidebar;

            vm.employeeLeaves = EmployeeLeaveService.employeeLeaves;
 
            vm.employeeLeave = null

            vm.viewLeaveDetails = function(id) {

                EmployeeLeaveService.getEmployeeLeave(id).then(function() {
                    vm.employeeLeave = EmployeeLeaveService.employeeLeave;
                    LxDialogService.open('viewLeaveRequestDialog');
                });
            };

            vm.updateLeaveRequest = function(status, id) {

                var title = (status == 'a') ? 'Approve Request' : 'Disapprove Request';
                var message = (status == 'a') ? 'Continue approve this request?' : 'Continue disapprove this request?';
                var success = (status == 'a') ? 'Leave Request Approved' : 'Leave Request Disapproved';

                LxNotificationService
                    .confirm(title, message, 
                        { 
                            cancel :'No', 
                            ok :'Yes' 
                        }, function(answer) {
                            if (answer) {

                                var params = {
                                    'leave_status' : status,
                                    'remarks' : vm.remarks,
                                    'id' : id
                                };

                                EmployeeLeaveService.updateEmployeeLeave(params).then(function() {
                                    EmployeeLeaveService.getEmployeeLeaveRequests().then(function(){
                                        vm.employeeLeaves = EmployeeLeaveService.employeeLeaves;
                                        LxNotificationService.success(success);

                                        try {
                                            LxDialogService.close('viewLeaveRequestDialog');
                                        } catch(e) {

                                        }
                                    })
                                });
                            }
                        }
                    );

            }

		}

})();