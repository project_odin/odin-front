;(function() {

    'use strict';

    angular
        .module('module.hr')
        .controller('TimesheetController', [
            '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   'LxNotificationService'
        ,   'LxDialogService'
        ,   'TimesheetService'
        ,   LeaveController]);

        function LeaveController(
            $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   LxNotificationService
        ,   LxDialogService
        ,   TimesheetService
        ) {

            var vm = this;

            vm.timesheet = TimesheetService.timesheet;

            console.log(vm.timesheet);

        }

})();