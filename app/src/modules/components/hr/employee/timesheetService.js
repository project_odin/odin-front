;(function() {

    'use strict';

    angular
        .module('module.hr')
        .service('TimesheetService', ['$resource', '$rootScope', TimesheetService]);

        function TimesheetService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {
                        getTimesheets : {
                            url : $rootScope.config.application_server + 'api/hr/timesheet',
                            method: 'GET',
                        }
                    }
                );

                var TimesheetService = {

                    timesheet : null,
                    employeeLeave  : null,
                    
                    getTimesheets : function () {
                        var self = this;
                        return api.getTimesheets().$promise.then(function(response) {
                            self.timesheet = response.timesheets;
                        })
                    }
                };

                return TimesheetService;
        }

})();