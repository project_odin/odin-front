;(function() {

    'use strict';

    angular
        .module('module.hr')
        .service('EmployeeService', ['$resource', '$rootScope', EmployeeService]);

        function EmployeeService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {
                        getEmployees : {
                            url : $rootScope.config.application_server + 'api/hr/employee',
                            method: 'GET',
                        },

                        getEmployee : {
                            url : $rootScope.config.application_server + 'api/hr/employee/:id',
                            method: 'GET',
                            params : {id : '@id'}
                        },

                        addEmployee : {
                            url : $rootScope.config.application_server + 'api/hr/employee',
                            method: 'POST'
                        },

                        updateEmployee : {
                            url : $rootScope.config.application_server + 'api/hr/employee/:id',
                            params : {id : '@id'},
                            method: 'PUT'
                        }
                    }
                );

                var EmployeeService = {

                    employees : null,
                    employee  : null,
                    
                    getEmployees : function () {
                        var self = this;
                        return api.getEmployees().$promise.then(function(response) {
                            self.employees = response.employees;
                        })
                    },

                    getEmployee  : function (id) {
                        var self = this;
                        return api.getEmployee({'id' : id}).$promise.then(function(response) {
                            self.employee = response.employee;
                            self.userData = response.user;
                        })
                    },

                    addEmployee  : function (params) {
                        return api.addEmployee(params).$promise.then(function(){

                        });
                    },

                    updateEmployee : function(params) {
                        return api.updateEmployee(params).$promise;
                    }
                };

                return EmployeeService;
        }

})();