;(function() {

	'use strict';

	angular
		.module('module.hr')
		.controller('EmployeeController', [
			'$auth'
		, 	'$http'
		, 	'$location'
		, 	'$window'
		, 	'$rootScope'
        ,   '$timeout'
		, 	'Sidebar'
		,	'EmployeeService'
        ,   'Upload'
		,	'LxDialogService'
		, 	'LxNotificationService'
        ,   'TimesheetService'
        ,   'Filters'
		, 	EmployeeController]);

		function EmployeeController(
			$auth
		, 	$http
		, 	$location
		, 	$window
		, 	$rootScope
        ,   $timeout
		, 	Sidebar
		, 	EmployeeService
        ,   Upload
		,	LxDialogService
		, 	LxNotificationService
        ,   TimesheetService
        ,   Filters
		) {

			var vm = this;

            vm.Sidebar = Sidebar;
            vm.filters = [];
            vm.employees = EmployeeService.employees;
            vm.display = 'employee';
            vm.showTable = true;
            vm.toggleList = function(table) {
                vm.display = '';

                $timeout(function() {
                    vm.display = table; 
                }, 500);
            }

            vm.openDialog = function(dialogId) {
                LxDialogService.open(dialogId);
            }

            Filters.getFilters({'filters[]' : ['positions', 'departments', 'tax_exemption']}).then(function(response) {
                vm.filters.positions      = response.filters.positions;
                vm.filters.departments    = response.filters.departments;
                vm.filters.tax_exemption  = response.filters.tax_exemption;
            });

            vm.addEmployee = function() {

                vm.employee.department_id = vm.employee.department.id;
                vm.employee.position_id = vm.employee.position.id;
                vm.employee.exemption_id = vm.employee.exemption.id;
                vm.loading = true;

                EmployeeService.addEmployee(vm.employee).then(function() {

                    vm.loading = false;
                    vm.employee = {};

                    LxDialogService.close('employeeDialog');
                    LxNotificationService.success('Employee successfully added');
                    vm.showTable = false;
                    EmployeeService.getEmployees().then(function() {
                        vm.employees = EmployeeService.employees;
                        vm.toggleList('employee');
                        vm.showTable = true;
                    })
                });
            }


            vm.uploadAttendance = function() {
                vm.loading = true;
                Upload.upload({
                    url     : $rootScope.config.application_server +  'api/hr/timesheet',
                    file    : vm.file
                }).progress(function (evt) {
                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                }).success(function (data, status, headers, config) { 
                    $rootScope.$broadcast('timesheetUploaded');
                    vm.display = 'timesheets'
                    LxDialogService.close('uploadAttendanceDialog');
                    LxNotificationService.success(data.success);
                    vm.loading = false;
                }).error(function (data, status, headers, config) {
                    LxNotificationService.error(data.error);
                });
            }

            vm.deleteEmployee = function (employeeId) {
                LxNotificationService
                    .confirm('Unpublish Opening', 'Continue unpublish opening?', 
                        { 
                            cancel :'No', 
                            ok :'Yes' 
                        }, function(answer) {
                            if (answer) {
                                OpeningService.unpublishJobPost(id).then(function() {
                                    vm.viewList = false;  
                                    OpeningService.getOpenings().then(function() {
                                        vm.viewList = true;  
                                        vm.openings = OpeningService.openings;
                                    });
                                });

                            }
                        }
                    );
            }

            vm.deactivateEmployee = function (employeeId) {
                LxNotificationService
                    .confirm('Unpublish Opening', 'Continue unpublish opening?', 
                        { 
                            cancel :'No', 
                            ok :'Yes' 
                        }, function(answer) {
                            if (answer) {
                                OpeningService.unpublishJobPost(id).then(function() {
                                    vm.viewList = false;  
                                    OpeningService.getOpenings().then(function() {
                                        vm.viewList = true;  
                                        vm.openings = OpeningService.openings;
                                    });
                                });

                            }
                        }
                    );
            }

            vm.activateEmployee = function (employeeId) {
                LxNotificationService
                    .confirm('Unpublish Opening', 'Continue unpublish opening?', 
                        { 
                            cancel :'No', 
                            ok :'Yes' 
                        }, function(answer) {
                            if (answer) {
                                OpeningService.unpublishJobPost(id).then(function() {
                                    vm.viewList = false;  
                                    OpeningService.getOpenings().then(function() {
                                        vm.viewList = true;  
                                        vm.openings = OpeningService.openings;
                                    });
                                });

                            }
                        }
                    );
            }


            vm.editEmployee = function(id) {

                EmployeeService.getEmployee(id).then(function(){
                    
                    vm.employee = {};
                    
                    var employee = EmployeeService.employee;

                    vm.employee.civil_status = (employee[0].civil_status === 's') ? 'Single' : 'Married'; 
                    vm.employee.gender = (employee[0].gender === 'm') ? 'Male' : 'Female'; 

                    vm.employee.id                 = employee[0].employee_id;
                    vm.employee.first_name         = employee[0].first_name;
                    vm.employee.last_name          = employee[0].last_name;
                    vm.employee.employee_number    = employee[0].employee_number;
                    vm.employee.contact_number     = employee[0].contact_number;
                    vm.employee.address            = employee[0].address;
                    vm.employee.account_number     = employee[0].account_number;
                    vm.employee.phil_health_number = employee[0].phil_health_number;
                    vm.employee.sss_number         = employee[0].sss_number;
                    vm.employee.tin_number         = employee[0].tin_number;
                    vm.employee.pagibig_number     = employee[0].pagibig_number;
                    vm.employee.birthdate          = employee[0].birth_date;

                    vm.selected_position   = {id : employee[0].position_id, position: employee[0].position};
                    vm.selected_department = {id : employee[0].department_id, department: employee[0].department};
                    vm.selected_exemption  = {id : employee[0].exemption_id, description: employee[0].description};

                    vm.employee.hire_date = employee[0].hire_date;
                    vm.employee.endo_date = employee[0].endo_date;

                    vm.toggleList('viewemployee');
                })

            }

            vm.saveEmployeeUpdate = function() {

                var data = {
                    id                 : vm.employee.id, 
                    contact_number     : vm.employee.contact_number,
                    address            : vm.employee.address,
                    account_number     : vm.employee.account_number,
                    phil_health_number : vm.employee.phil_health_number,
                    sss_number         : vm.employee.sss_number,
                    tin_number         : vm.employee.tin_number,
                    pagibig_number     : vm.employee.pagibig_number,
                    hire_date          : vm.employee.hire_date,
                    endo_date          : vm.employee.endo_date,
                    department_id      : vm.selected_position.id,
                    position_id        : vm.selected_position.id,
                    exemption_id       : vm.selected_exemption.id
                }

                EmployeeService.updateEmployee(data).then(function(){
                    vm.showTable = false;
                    EmployeeService.getEmployees().then(function() {
                        vm.employees = EmployeeService.employees;
                        vm.showTable = true;
                        LxNotificationService.success('Update employee success.')
                    })
                })

            }
        }

})();


