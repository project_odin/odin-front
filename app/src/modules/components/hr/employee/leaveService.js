;(function() {

    'use strict';

    angular
        .module('module.hr')
        .service('EmployeeLeaveService', ['$resource', '$rootScope', EmployeeLeaveService]);

        function EmployeeLeaveService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {
                        getEmployeeLeaveRequests : {
                            url : $rootScope.config.application_server + 'api/hr/leave',
                            method: 'GET',
                        },

                        getEmployeeLeave : {
                            url : $rootScope.config.application_server + 'api/hr/leave/:id',
                            method: 'GET',
                            params : {id : '@id'}
                        },

                        updateEmployeeLeave : {
                            url : $rootScope.config.application_server + 'api/hr/leave/:id',
                            params : {id : '@id'},
                            method: 'PUT'
                        },
                    }
                );

                var EmployeeLeaveService = {

                    employeeLeaves : null,
                    employeeLeave  : null,
                    
                    getEmployeeLeaveRequests : function () {
                        var self = this;
                        return api.getEmployeeLeaveRequests().$promise.then(function(response) {
                            self.employeeLeaves = response.requests;
                        })
                    },

                    getEmployeeLeave  : function (id) {
                        var self = this;
                        return api.getEmployeeLeave({'id' : id}).$promise.then(function(response) {
                            self.employeeLeave = response.request;
                        })
                    },

                    updateEmployeeLeave  : function (params) {
                        return api.updateEmployeeLeave(params).$promise;
                    },
                };

                return EmployeeLeaveService;
        }

})();