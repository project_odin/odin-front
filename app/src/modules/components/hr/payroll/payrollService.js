;(function() {

    'use strict';

    angular
        .module('module.hr')
        .service('PayrollService', ['$resource', '$rootScope', PayrollService]);

        function PayrollService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {
                        getContribution : {
                            url : $rootScope.config.application_server + 'api/hr/payroll/contribution/:salary/:type',
                            method: 'GET',
                            params : {salary : '@salary', type : '@type'}
                        },

                        getWithholdingTax : {
                            url : $rootScope.config.application_server + 'api/hr/payroll/tax',
                            method: 'POST',
                        },

                        getEmployeeDetails : {
                            url : $rootScope.config.application_server + 'api/hr/payroll/employee',
                            method: 'POST',

                        },

                        savePayroll : {
                            url : $rootScope.config.application_server + 'api/hr/payroll',
                            method: 'POST',
                        },

                        getPayrollSummary : {
                            url : $rootScope.config.application_server + 'api/hr/payroll',
                            method: 'GET',
                        }
                    }
                );

                var PayrollService      = {};
                var hoursPerDay         = 8;
                var ECOLA               = 15;
                var ONE_THIRTY          = 130;
                var ONE_TENTY_FIVE      = 125;
                var TWO_HUNDRED         = 200;
                var TEN                 = 10; 

                var BASIC                = 466;
                var RATE_PER_HOUR        = BASIC / hoursPerDay;
                var OVERTIME_RATE        = ((BASIC * ONE_TENTY_FIVE) / hoursPerDay) / 100;
                var REST_DAY_OT_RATE     = (((BASIC * ONE_THIRTY) + ECOLA) / hoursPerDay) / 100;
                var SPECIAL_HOLIDAY_RATE = ((((BASIC * ONE_THIRTY) / 100) + ECOLA) / hoursPerDay);
                var REGULAR_HOLIDAY_RATE = ((BASIC + ECOLA) * TWO_HUNDRED) / 2 / hoursPerDay / 100;
                var NIGHT_DIFF_RATE      = ((BASIC * TEN) / 8) / 100;
                var LATE_UNDERTIME_RATE  = (BASIC / hoursPerDay / 60);

                var PayrollData = {

                    overtimePay  : 0, 
                    nightdiffPay : 0,
                    specialPay   : 0,
                    legalPay     : 0,
                    totalbasic   : 0,
                    basicPay     : BASIC,
                    ratePerHour  : RATE_PER_HOUR,
                    allowancePay : null,
                    totalecola   : 0,

                    'overtime' : function(hours) {
                        hours = (!isNaN(hours)) ? hours : 0;
                        this.overtimePay = OVERTIME_RATE * hours
                        return this.overtimePay;
                    },

                    'nightdiff' : function(hours) {
                        hours = (!isNaN(hours)) ? hours : 0;
                        this.nightdiffPay = NIGHT_DIFF_RATE * hours
                        return this.nightdiffPay;
                    },

                    'special' : function(hours) {
                        hours = (!isNaN(hours)) ? hours : 0;
                        this.specialPay = SPECIAL_HOLIDAY_RATE * hours
                        return this.specialPay;
                    },

                    'legal' : function(hours) {
                        hours = (!isNaN(hours)) ? hours : 0;
                        this.legalPay = REGULAR_HOLIDAY_RATE * hours
                        return this.legalPay;
                    },

                    'late' : function(hours) {
                        hours = (!isNaN(hours)) ? hours : 0;
                        return LATE_UNDERTIME_RATE * hours;
                    },

                    'basic' : function(hours) {
                        hours = (!isNaN(hours)) ? hours : 0;
                        this.totalbasic = RATE_PER_HOUR * hours;
                        return this.totalbasic;
                    },

                    'ecola' : function(hours) {
                        hours = (!isNaN(hours)) ? hours : 0;
                        this.totalecola = (hours / hoursPerDay) * ECOLA;
                        return this.totalecola;
                    },

                    'allowance' : function(amount) {
                        amount = (!isNaN(amount)) ? amount : 0;
                        this.allowancePay = amount;
                    },

                    'totalearning' : function() {
                        return this.overtimePay + 
                            this.nightdiffPay + 
                            this.specialPay + 
                            this.legalPay + 
                            this.totalbasic + 
                            this.allowancePay + 
                            this.totalecola;     
                    },

                    'sss' : function(salary) {
                        var params = {
                            salary : salary,
                            type : 'sss'
                        };

                        return api.getContribution(params).$promise;
                    },    
                };

                var PayrollService = {

                    contribution : null,
                    employee : null,
                    payrollSummary : null,

                    getContribution : function(salary, type) {
                        
                        var self = this;

                            var params = {
                                salary : salary,
                                type : type
                            };

                            return api.getContribution(params).$promise.then(function(response) {
                                self.contribution = response.contribution[0].contribution;
                            });
                    },

                    getWithholdingTax : function(taxable, employee) {
                        return api.getWithholdingTax({'amount' : taxable, 'employee' : employee}).$promise;
                    },

                    getEmployeeDetails : function(params) {
                            
                        var self = this;

                        return api.getEmployeeDetails(params).$promise.then(function(response) {
                            self.employee = response.employee;
                        })
                    },

                    savePayroll : function(params) {
                        return api.savePayroll(params).$promise;
                    },

                    getPayrollSummary : function() {
                            
                        var self = this;

                        return api.getPayrollSummary().$promise.then(function(response) {
                            self.payrollSummary = response.summary;
                        })
                    }

                };

                PayrollService.PayrollData = PayrollData;

                return PayrollService;
        }

})();