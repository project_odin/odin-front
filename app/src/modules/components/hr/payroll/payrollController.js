;(function() {

    'use strict';

    angular
        .module('module.hr')
        .controller('PayrollController', [
            '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   '$filter'
        ,   'Sidebar'
        ,   'PayrollService'
        ,   '$timeout'
        ,   'LxNotificationService'
        ,   PayrollController]
        )
        .filter('dateDiff', ['$parse', dateDiff]);

        function dateDiff ($parse) {

            var magicNumber = (1000 * 60 * 60 * 24);

            return function (toDate, fromDate) {
                if(toDate && fromDate){
                    var dayDiff = Math.floor((toDate - fromDate) / magicNumber);

                    if (!isNaN(dayDiff)) {
                        if (angular.isNumber(dayDiff)){
                            return dayDiff + 1;
                        } else {
                            return 0;
                        }
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            };
        }

        function PayrollController(
            $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   $filter
        ,   Sidebar
        ,   PayrollService
        ,   $timeout
        ,   LxNotificationService
        ) {

            var vm = this;
            
            vm.Sidebar = Sidebar;

            vm.ctrlData = {
                activeTab : 1
            }

            vm.displayComputation = false;
            vm.loading = false;
            vm.payroll = {};
            vm.display = 'summary';

            vm.filter = {};

            vm.date = vm.filter;

            vm.toggleList = function(table) {
                vm.display = '';

                $timeout(function() {
                    vm.display = table; 
                }, 500);
            }
            vm.savePayroll = function() {

                vm.payroll.from = $filter('date')(vm.payroll.from, 'yyyy-MM-dd');
                vm.payroll.to   = $filter('date')(vm.payroll.to, 'yyyy-MM-dd');
                
                PayrollService.savePayroll(vm.payroll).then(function() {
                    LxNotificationService.success("Paroll saved");
                    // vm.payroll = null;
                });
            }

                vm.payrollSummary = PayrollService.payrollSummary;

            vm.getEmployee = function() {

                if (vm.payroll.employee_number.length === 9) {

                    var params = {
                        'date_from' : $filter('date')(vm.payroll.from, 'yyyy-MM-dd'),
                        'date_to' : $filter('date')(vm.payroll.to, 'yyyy-MM-dd'),
                        'employee' : vm.payroll.employee_number
                    }


                    PayrollService.getEmployeeDetails(params).then(function() {
                        var employee = PayrollService.employee;

                         vm.employee = employee;   

                        if (typeof(employee) === 'object') {
                            vm.payroll.employee_name = employee.first_name + ' ' + employee.last_name;
                            vm.payroll.basic_pay     = $filter('number')(employee.amount,2);
                            vm.payroll.rate_per_hour = $filter('number')(employee.amount / 8,2);
                            vm.payroll.hours         = $filter('number')(employee.hours_rendered,2);
                            vm.payroll.total_basic   = $filter('number')(PayrollService.PayrollData['basic'](vm.payroll.hours), 2);  

                            vm.payroll.overtime            = $filter('number')(employee.total_overtime,2);
                            vm.payroll.night_diff          = $filter('number')(employee.total_night_diff,2);
                            vm.payroll.special_holiday_pay = $filter('number')(employee.special_holiday_pay, 2);
                            vm.payroll.legal_holiday_pay   = $filter('number')(employee.regular_holiday_pay, 2);

                            vm.payroll.overtime_pay        = $filter('number')(PayrollService.PayrollData['overtime'](vm.payroll.overtime), 2);
                            vm.payroll.night_diff_pay      = $filter('number')(PayrollService.PayrollData['nightdiff'](vm.payroll.night_diff), 2);
                            vm.payroll.ecola               = $filter('number')(PayrollService.PayrollData['ecola'](vm.payroll.hours),2);
                            
                            var total  = 0;
                            var totaldeduction = 0;
                            
                            total += parseFloat(vm.payroll.total_basic.replace(",", "")); 
                            total += parseFloat(vm.payroll.overtime_pay.replace(",", "")); 
                            total += parseFloat(vm.payroll.night_diff_pay.replace(",", "")); 
                            total += parseFloat(vm.payroll.special_holiday_pay.replace(",", ""));
                            total += parseFloat(vm.payroll.legal_holiday_pay.replace(",", ""));
                            total += parseFloat(vm.payroll.ecola.replace(",", ""));

                            vm.payroll.total_earning       = $filter('number')(total,2);;

                            var earning = parseFloat(vm.payroll.total_earning.replace(",", ""));

                            if ( earning > 1000) {
                                
                                PayrollService.getContribution(vm.payroll.total_earning, 'sss').then(function() {
                                    
                                    earning -= parseFloat(PayrollService.contribution.replace(",", ""));
                                    vm.payroll.sss = $filter('number')(PayrollService.contribution,2);
                                    totaldeduction += parseFloat(PayrollService.contribution.replace(",", ""));
                                    PayrollService.getContribution(vm.payroll.total_earning, 'ph').then(function() {
                                        vm.payroll.philhealth  = $filter('number')(PayrollService.contribution,2);
                                        earning -= PayrollService.contribution; 
                                        totaldeduction += parseFloat(PayrollService.contribution.replace(",", ""));
                                        var multiplier = (earning > 1500) ? .02 : .01;
                                        var hdmf = earning * multiplier;

                                        totaldeduction += hdmf;

                                        vm.payroll.hdmf = $filter('number')( hdmf,2);
                                        earning -= hdmf;

                                        PayrollService.getWithholdingTax(earning, vm.employee.employee_number).then(function(response) {
                                            vm.payroll.tax = $filter('number')(response.tax,2);
                                            totaldeduction += response.tax;
                                            console.log(totaldeduction);

                                            vm.payroll.totaldeduction = $filter('number')(totaldeduction,2);



                                        })

                                    });

                                });

                            } else {
                                vm.payroll.sss = $filter('number')(0,2);
                                vm.payroll.philhealth = $filter('number')(0,2);
                            }
                            
                            vm.displayComputation = true;
                            vm.loading = true;
                        } else {
                            LxNotificationService.error('Employee Not found!');
                            vm.displayComputation = false;
                        }

                    });
                }
            }
        }
})();