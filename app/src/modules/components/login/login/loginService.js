;(function() {

    'use strict';

    angular
        .module('module.login')
        .service('LoginService', ['$resource', '$rootScope', '$window', LoginService]);

        function LoginService ($resource, $rootScope, $window) {

                var api = $resource ('',
                    {},
                    {
                        resetPassword : {
                            url : $rootScope.config.application_server + '/api/user/reset-password/:id',
                            params : {id : '@id'},
                            method: 'GET'
                        }
                    }
                );

                var LoginService = {
                    resetPassword : function(id) {
                        var self = this;
                        return api.resetPassword({'id' : id}).$promise;
                    },
                };

                return LoginService;
        }

})();