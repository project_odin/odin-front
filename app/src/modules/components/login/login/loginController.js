;(function() {

	'use strict';

	angular
		.module('module.login')
		.controller('LoginController', ['$auth', '$http', '$location', '$window', '$rootScope', 
			'LxNotificationService', 
			'LoginService',
			LoginController]);

		function LoginController($auth, $http, $location, $window, $rootScope, LxNotificationService, LoginService) {
			
			var vm = this;
	        
	        vm.showMessage = false;

		    vm.logout = function() {
	            $auth.logout().then(function() {
             		$rootScope.$broadcast('loggedOut');
                    $location.path('/login');
        		});
	        }

	        vm.login = function() {

	        	vm.loading = true;

	            var credentials = {
	                username  : vm.username,
	                password  : vm.password
	            }
	            
	            $auth.login(credentials).then(function (data) {
					
					return $http.get($rootScope.config.application_server + 'api/authenticate/user');

	            }, function (error) {

	            	LxNotificationService.error(error.data.error);

	            }).then(function(response) {

	            	if (typeof response !== 'undefined') {
	            		$window.localStorage.user = response.data.user.id;
	            		
	            		$window.localStorage.user_type = response.data.user.client_id;

	            		$location.path('/' + response.data.user.client_id + '/dashboard');
	            	}

	            });
	        }

	        vm.resetPassword = function() {
	        	vm.loading = true;
	        	LoginService.resetPassword(vm.request_username).then(function(response) {
	        		LxNotificationService.success('Change password success. Please check your email.');
					$location.path('login');
	        	})
	        }

		}

})();