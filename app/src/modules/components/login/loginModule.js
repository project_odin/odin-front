;(function() {

    'use strict';

    angular
        .module('module.login', ['ngRoute', 'satellizer'])
        .config(['$routeProvider', '$authProvider', function($routeProvider, $authProvider) {

            $authProvider.loginUrl = 'http://api.apservices.com/api/authenticate';

            var authenticate = ['AuthenticationService', function(AuthenticationService) {
                return AuthenticationService.checkAuth();
            }];

           $routeProvider
                .when('/login', {
                    templateUrl     : 'src/modules/components/login/login/loginView.html',
                    controller      : 'LoginController',
                    controllerAs    : 'vm',
                    resolve : {
                        checkAuth : authenticate
                    }
                })
                .when('/password-reset', {
                    templateUrl     : 'src/modules/components/login/login/forgotpassword.html',
                    controller      : 'LoginController',
                    controllerAs    : 'vm'
                });

        }
    ]);

})();