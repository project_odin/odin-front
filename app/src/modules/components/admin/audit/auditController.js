;(function() {

	'use strict';

	angular
		.module('module.admin')
		.controller('AuditTrailController', ['$auth', '$http', '$location', '$window', '$rootScope', 'Sidebar', 'AuditService', AuditTrailController]);

		function AuditTrailController($auth, $http, $location, $window, $rootScope, Sidebar, AuditService) {

			var vm = this;
			
			vm.Sidebar = Sidebar;

        	AuditService.getAuditLogs().then(function() {
				vm.logs = AuditService.logs;
        	});

		}

})();