;(function() {

    'use strict';

    angular
        .module('module.admin')
        .service('AuditService', ['$resource', '$rootScope', AuditService]);

        function AuditService ($resource, $rootScope) {

                var api = $resource (
                    '',
                    {},
                    {
                        getAuditLogs : {
                            url : $rootScope.config.application_server + 'api/audit',
                            method : 'GET'
                        }
                    }
                );


                var AuditService = {
                    logs : null,
                    getAuditLogs : function getAudits() {

                        var self = this;
                        return api.getAuditLogs().$promise.then( function(response) {
                            self.logs = response.audit;
                        })
                    }
                }

                return AuditService;
        }

})();