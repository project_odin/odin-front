;(function() {

    'use strict';

    angular
        .module('module.admin')
        .service('UserService', ['$resource', '$rootScope', UserService]);

        function UserService ($resource, $rootScope) {

                var api = $resource (
                    'http://recruitment.mis.com/',
                    {},
                    {
                        getUsers : {
                            url : $rootScope.config.application_server + 'api/admin/users',
                            method : 'GET'
                        },

                        addUser : {
                            url : $rootScope.config.application_server + 'api/admin/users',
                            method : 'POST' 
                        },

                        deleteUser : {
                            url : $rootScope.config.application_server + 'api/admin/users/:id',
                            method : 'DELETE',
                            params : {'id' : '@id'}
                        }
                    }
                );


                var UserService = {
                    
                    users : null,

                    getUsers : function getUsers() {

                        var self = this;

                        return api.getUsers().$promise.then( function(response) {
                            self.users = response.users;
                        })
                    },

                    addUser : function addUser(params) {
                        return api.addUser(params).$promise;
                    },

                    deleteUser : function deleteUser(userId) {
                        return api.deleteUser({'id' : userId}).$promise;
                    }
                };

                return UserService;
        }

})();