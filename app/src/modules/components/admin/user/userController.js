;(function() {

    'use strict';

    angular
        .module('module.admin')
        .controller('UserController', ['$auth', '$http', '$location', '$window', '$rootScope', 'Sidebar', 'UserService', 'LxDialogService', 'LxNotificationService', UserController]);

    function UserController($auth, $http, $location, $window, $rootScope, Sidebar, UserService, LxDialogService, LxNotificationService) {

        var vm = this;
        
        vm.Sidebar = Sidebar;

        vm.users = UserService.users;

        vm.loading = false;
        
        vm.openDialog = function(dialogId) {
            LxDialogService.open(dialogId);
        }        

        vm.userTypes = [
            { id: 'admin',      type: 'Administrator'},
            { id: 'hr',         type: 'Human Resource'},
            { id: 'accounting', type: 'Accounting'},
        ];

        vm.addUser = function() {
            
            vm.loading = true;

            vm.user.user_type = vm.selected_user_type.id;

            UserService.addUser(vm.user).then(function() {

                vm.loading = false;
                vm.user = {};
                vm.selected_user_type = null;

                LxDialogService.close('addUserDialog');
                LxNotificationService.success('User successfully added');

                UserService.getUsers().then(function() {
                    vm.users = UserService.users;
                })


            }, function(error) {

                var errors = error.data
                angular.forEach(errors, function(value, key) {
                    LxNotificationService.error(value);
                });

                vm.loading = false;

            });
        }

        vm.showEditDialog = function() {

        }

        vm.deleteUser = function(userId) {

            LxNotificationService
                .confirm('Delete User', 'Continue delete user?', 
                    { 
                        cancel :'No', 
                        ok :'Yes' 
                    }, function(answer) {

                        if (answer) {
                            
                            UserService.deleteUser(userId).then(function() {
                                UserService.getUsers().then(function(){
                                    vm.users = UserService.users;  
                                    LxNotificationService.info('User deleted'); 
                                });
                            });

                        }
                    }
                );
        }
    }
})();