;(function() {

    'use strict';

    angular
        .module('module.admin')
        .service('CompaniesService', ['$resource', '$rootScope', CompaniesService]);

        function CompaniesService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {   
                        getCompanies : {
                            url : $rootScope.config.application_server + 'api/admin/company',
                            method : 'GET'
                        },

                        createCompany : {
                            url : $rootScope.config.application_server + 'api/admin/company',
                            method : 'POST'
                        },

                        getCompany : {
                            url : $rootScope.config.application_server + 'api/admin/company/:id',
                            method : 'GET',
                            params : {'id' : '@id'}
                        },

                        updateCompany : {
                            url : $rootScope.config.application_server + 'api/admin/company/:id',
                            method : 'PUT',
                            params : {'id' : '@id'}
                        },

                        deleteCompany : {
                            url : $rootScope.config.application_server + 'api/admin/company/:id',
                            method : 'DELETE',
                            params : {'id' : '@id'}
                        }
                    }
                );


                var CompaniesService = {

                    company : null,

                    companies : null,

                    getCompanies  : function () {
                        
                        var self = this;

                        return api.getCompanies().$promise.then(function(response) {
                            self.companies = response.companies;
                        });
                    }, 

                    createCompany : function (data) {
                        return api.createCompany(data).$promise;
                    },

                    getCompany    : function (id) {

                        var self = this;

                        return api.getCompany({'id' : id}).$promise.then(function(response) {
                            self.company = response.company;
                        });
                    },

                    updateCompany : function (data, id) {

                        data.id = id;

                        return api.updateCompany(data).$promise;
                    },

                    deleteCompany : function (id) {
                        return api.deleteCompany({'id' : id}).$promise;
                    } 
                };

                return CompaniesService;
        }

})();