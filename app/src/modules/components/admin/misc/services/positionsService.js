;(function() {

    'use strict';

    angular
        .module('module.admin')
        .service('PositionsService', ['$resource', '$rootScope', PositionsService]);

        function PositionsService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {   
                        getPositions : {
                            url : $rootScope.config.application_server + 'api/admin/position',
                            method : 'GET'
                        },

                        createPosition : {
                            url : $rootScope.config.application_server + 'api/admin/position',
                            method : 'POST'
                        },

                        getPosition : {
                            url : $rootScope.config.application_server + 'api/admin/position/:id',
                            method : 'GET',
                            params : {'id' : '@id'}
                        },

                        updatePosition : {
                            url : $rootScope.config.application_server + 'api/admin/position/:id',
                            method : 'PUT',
                            params : {'id' : '@id'}
                        },

                        deletePosition : {
                            url : $rootScope.config.application_server + 'api/admin/position/:id',
                            method : 'DELETE',
                            params : {'id' : '@id'}
                        }
                    }
                );


                var PositionsService = {

                    position : null,

                    positions : null,

                    getPositions  : function () {
                        
                        var self = this;

                        return api.getPositions().$promise.then(function(response) {
                            self.positions = response.positions;
                        });
                    }, 

                    createPosition : function (data) {
                        return api.createPosition(data).$promise;
                    },

                    getPosition : function (id) {

                        var self = this;

                        return api.getPosition({'id' : id}).$promise.then(function(response) {
                            self.position = response.position;
                        });
                    },

                    updatePosition : function (data, id) {

                        data.id = id;

                        return api.updatePosition(data).$promise;
                    },

                    deletePosition : function (id) {
                        return api.deletePosition({'id' : id}).$promise;
                    } 
                };

                return PositionsService;
        }

})();