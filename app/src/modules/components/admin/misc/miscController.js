;(function() {

    'use strict';

    angular
        .module('module.admin')
        .controller('MiscController', [
            '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   'Sidebar'
        ,   'CompaniesService'
        ,   'PositionsService'
        ,   'LxDialogService'
        ,   'LxNotificationService'
        ,   MiscController]);

        function MiscController(
            $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   Sidebar
        ,   CompaniesService
        ,   PositionsService
        ,   LxDialogService
        ,   LxNotificationService

        ) {

            var vm = this;
        
            vm.Sidebar   = Sidebar;

            CompaniesService.getCompanies().then(function() {
                vm.companies = CompaniesService.companies;
            });

            vm.company   = null;
            vm.view      = 'add';

            vm.openDialog = function(dialogId) {
                LxDialogService.open(dialogId);
            }

            vm.addCompany = function() {
                
                vm.loading = true;

                CompaniesService.createCompany(vm.company)
                    .then(function() {
                        CompaniesService.getCompanies().then(function() {
                            vm.companies = CompaniesService.companies;
                            vm.loading = false;
                            vm.company = {};
                            LxDialogService.close('addCompanyDialog');
                        });
                    }, function(error) {

                        var errors = error.data
                        angular.forEach(errors, function(value, key) {
                            LxNotificationService.error(value);
                        });

                        vm.loading = false;

                    });
            }

            vm.showEditDialog = function(dialog, id) {

                CompaniesService.getCompany(id)
                    .then(function() {
                        vm.view = 'edit';
                        vm.company = CompaniesService.company;
                        LxDialogService.open(dialog);
                    });

            }

            vm.editCompany = function() {

                vm.loading = true;

                CompaniesService.updateCompany(vm.company, vm.company.id)
                    .then(function() {
                        return CompaniesService.getCompanies();
                    }).then(function() {
                        vm.companies = CompaniesService.companies;
                        vm.loading = false;
                        vm.company = {};
                        LxDialogService.close('addCompanyDialog');
                    });
            }

            vm.deleteCompany = function(id) {

                LxNotificationService
                    .confirm('Delete Company', 'Continue delete company?', 
                        { 
                            cancel :'No', 
                            ok :'Yes' 
                        }, function(answer) {

                            if (answer) {

                                CompaniesService.deleteCompany(id)
                                    .then(function() {
                                        return CompaniesService.getCompanies();
                                    }).then(function() {
                                        vm.companies = CompaniesService.companies;
                                        vm.loading = false;
                                    });
                            }
                        }
                    );
            }

            /**
             * Positions Management Script
             */
            PositionsService.getPositions().then(function() {
                vm.positions = PositionsService.positions;
            })
            
            vm.position   = null;
            vm.view      = 'add';

            vm.addPosition = function() {
                
                vm.loading = true;

                PositionsService.createPosition(vm.position)
                    .then(function() {
                        PositionsService.getPositions().then(function() {
                            vm.positions = PositionsService.positions;
                            vm.loading = false;
                            vm.position = {};
                            LxDialogService.close('addPositionDialog');
                        });
                    }, function(error) {

                        var errors = error.data
                        angular.forEach(errors, function(value, key) {
                            LxNotificationService.error(value);
                        });

                        vm.loading = false;

                    });
            }

            vm.showEditPosition = function(dialog, id) {

                PositionsService.getPosition(id)
                    .then(function() {
                        vm.view = 'edit';
                        vm.position = PositionsService.position;
                        LxDialogService.open(dialog);
                    });

            }

            vm.editPosition = function() {

                vm.loading = true;

                PositionsService.updatePosition(vm.position, vm.position.id)
                    .then(function() {
                        return PositionsService.getPositions();
                    }).then(function() {
                        vm.positions = PositionsService.positions;
                        vm.loading = false;
                        vm.position = {};
                        LxDialogService.close('addPositionDialog');
                    });
            }

            vm.deletePosition = function(id) {

                LxNotificationService
                    .confirm('Delete Position', 'Continue delete position?', 
                        { 
                            cancel :'No', 
                            ok :'Yes' 
                        }, function(answer) {

                            if (answer) {

                                PositionsService.deletePosition(id)
                                    .then(function() {
                                        return PositionsService.getPositions();
                                    }).then(function() {
                                        vm.positions = PositionsService.positions;
                                        vm.loading = false;
                                    });
                            }
                        }
                    );
            }

    }
})();