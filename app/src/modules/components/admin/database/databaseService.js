;(function() {

    'use strict';

    angular
        .module('module.admin')
        .service('BackupService', ['$resource', '$rootScope', '$window', BackupService]);

        function BackupService ($resource, $rootScope, $window) {

                var api = $resource (
                    '',
                    {},
                    {
                        backupDatabase : {
                            url : $rootScope.config.application_server + 'api/db/backup',
                            method : 'POST'
                        },

                        restoreDatabase : {
                            url : $rootScope.config.application_server + 'api/db/restore',
                            method : 'POST' 
                        },

                        getDbLogs : {
                            url : $rootScope.config.application_server + 'api/backup',
                            method : 'GET'
                        }
                    }
                );


                var BackupService = {

                    dblogs : null,

                    backupDatabase : function backupDatabase() {
                        var params = {
                            'id' : $window.localStorage.user,
                            'type' : 'backup'
                        }
                        return api.backupDatabase(params).$promise;
                    },

                    restoreDatabase : function restoreDatabase(params) {
                        var params = {
                            'user' : $window.localStorage.user,
                            'type' : 'restore'
                        }
                        return api.restoreDatabase(params).$promise;
                    },

                    getDbLogs : function getDbLogs() {
                        var self = this;
                        return api.getDbLogs().$promise.then(function(response) {
                            self.dbLogs = response.dblogs;
                        })
                    }
                };

                return BackupService;
        }

})();