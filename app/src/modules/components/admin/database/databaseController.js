;(function() {

	'use strict';

	angular
		.module('module.admin')
		.controller('DatabaseController', ['$auth', '$http', '$location', '$window', '$rootScope', 'Sidebar', 'BackupService', 'LxNotificationService', DatabaseController]);

		function DatabaseController($auth, $http, $location, $window, $rootScope, Sidebar, BackupService, LxNotificationService) {

			var vm = this;
			
			vm.Sidebar = Sidebar;

            BackupService.getDbLogs().then(function() {
				vm.backupInfo = BackupService.dbLogs;
            });

			vm.backup = function() {


				 LxNotificationService
	                .confirm('Backup Database', 'Continue backup current data in database?', 
	                    { 
	                        cancel :'No', 
	                        ok :'Yes' 
	                    }, function(answer) {

	                        if (answer) {

	                            BackupService.backupDatabase().then(function() {
									$window.location.href = $rootScope.config.application_server + 'public/db_odin.sql';
	                            	LxNotificationService.info('Database backup success!'); 
	                            });
	                        }
	                    }
	                );
			}

			vm.restore = function() {
				 LxNotificationService
	                .confirm('Restore Database', 'Continue restore data in database? This action will restore data from the last backup date, transactions after the last backup will not be included.', 
	                    { 
	                        cancel :'No', 
	                        ok :'Yes' 
	                    }, function(answer) {

	                        if (answer) {

	                            BackupService.restoreDatabase().then(function() {
	                            	LxNotificationService.info('Database restored!'); 
	                            });
	                        }
	                    }
	                );
			}

		}

})();