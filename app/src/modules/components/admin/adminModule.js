;(function() {

    'use strict';

    angular
        .module('module.admin', ['ngRoute', 'satellizer', 'module.shared', 'smart-table'])
        .config(['$routeProvider', '$authProvider', function($routeProvider, $authProvider) {

            var authenticate = ['AuthenticationService', function(AuthenticationService) {
                return AuthenticationService.checkAuth();
            }];


           $routeProvider
                .when('/admin/dashboard', {
                    templateUrl     : 'src/modules/components/admin/dashboard/dashboardView.html',
                    controller      : 'DashboardController',
                    controllerAs    : 'vm',
                    module          : 'admin',
                    resolve         : {
                        DashboardController: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.admin",
                                    files: [
                                        "src/modules/components/admin/dashboard/dashboardController.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/admin/users', {
                    templateUrl     : 'src/modules/components/admin/user/userView.html',
                    controller      : 'UserController',
                    controllerAs    : 'vm',
                    module          : 'admin',
                    resolve         : {
                        UserController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.admin",
                                    files: [
                                        "src/modules/components/admin/user/userController.js",
                                        "src/modules/components/admin/user/userService.js"
                                    ]
                                }
                            ]).then(function() {
                                var UserService = $injector.get('UserService');
                                return UserService.getUsers()
                            });
                        }],
                        checkAuth   : authenticate,

                    }
                })
                .when('/admin/audit', {
                    templateUrl     : 'src/modules/components/admin/audit/auditView.html',
                    controller      : 'AuditTrailController',
                    controllerAs    : 'vm',
                    module          : 'admin',
                    resolve         : {
                        AuditTrailController: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.admin",
                                    files: [
                                        "src/modules/components/admin/audit/auditController.js",
                                        "src/modules/components/admin/audit/auditService.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/admin/database', {
                    templateUrl     : 'src/modules/components/admin/database/databaseView.html',
                    controller      : 'DatabaseController',
                    controllerAs    : 'vm',
                    module          : 'admin',
                    resolve         : {
                        DashboardController: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.admin",
                                    files: [
                                        "src/modules/components/admin/database/databaseController.js",
                                        "src/modules/components/admin/database/databaseService.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/admin/misc', {
                    templateUrl     : 'src/modules/components/admin/misc/miscView.html',
                    controller      : 'MiscController',
                    controllerAs    : 'vm',
                    module          : 'admin',
                    resolve         : {
                        MiscController: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.admin",
                                    files: [
                                        "src/modules/components/admin/misc/miscController.js",
                                        "src/modules/components/admin/database/databaseController.js",
                                        "src/modules/components/admin/database/databaseService.js",
                                        "src/modules/components/admin/misc/services/companiesService.js",
                                        "src/modules/components/admin/misc/services/companiesService.js",
                                        "src/modules/components/admin/misc/services/positionsService.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/admin/account', {
                    templateUrl     : 'src/modules/shared/account/accountView.html',
                    controller      : 'AccountController',
                    controllerAs    : 'vm',
                    module          : 'admin',
                    resolve         : {
                        AccountController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.shared",
                                    files: [
                                        "src/modules/shared/account/accountController.js",
                                        "src/modules/shared/account/accountService.js"
                                    ]
                                }
                            ]).then(function() {
                                var AccountService = $injector.get('AccountService');
                                return AccountService.getUser()

                            });
                        }],
                        checkAuth   : authenticate
                    }
                });

        }
    ]);

})();