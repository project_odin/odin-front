;(function() {

    'use strict';

    angular
        .module('module.candidate', ['ngRoute', 'satellizer', 'module.shared'])
        .config(['$routeProvider', '$authProvider', function($routeProvider, $authProvider) {

            var authenticate = ['AuthenticationService', function(AuthenticationService) {
                return AuthenticationService.checkAuth();
            }];


           $routeProvider
                .when('/register', {
                    templateUrl     : 'src/modules/components/candidate/register/registerView.html',
                    controller      : 'RegisterController',
                    controllerAs    : 'vm'
                })
                .when('/applicant/dashboard', {
                    templateUrl     : 'src/modules/components/candidate/dashboard/dashboardView.html',
                    controller      : 'CandidateDashboardController',
                    controllerAs    : 'vm',
                    module          : 'applicant',
                    resolve         : {
                        CandidateDashboardController: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.candidate",
                                    files: [
                                        "src/modules/components/candidate/dashboard/dashboardController.js",
                                        "src/modules/components/candidate/dashboard/dashboardService.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth           : authenticate
                    }
                })
                .when('/applicant/resume', {
                    templateUrl     : 'src/modules/components/candidate/resume/resumeView.html',
                    controller      : 'ResumeController',
                    controllerAs    : 'vm',
                    module          : 'applicant',
                    resolve         : {
                        ResumeController: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.candidate",
                                    files: [
                                        "src/modules/components/candidate/resume/resumeController.js",
                                        "src/modules/components/candidate/resume/resumeService.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/applicant/applications', {
                    templateUrl     : 'src/modules/components/candidate/applications/applicationsView.html',
                    controller      : 'ApplicationsController',
                    controllerAs    : 'vm',
                    module          : 'applicant',
                    resolve         : {
                        DashboardController: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.candidate",
                                    files: [
                                        "src/modules/components/candidate/applications/applicationsController.js",
                                        "src/modules/components/candidate/applications/applicationsService.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/employee/account', {
                    templateUrl     : 'src/modules/shared/account/accountView.html',
                    controller      : 'AccountController',
                    controllerAs    : 'vm',
                    module          : 'employee',
                    resolve         : {
                        AccountController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.shared",
                                    files: [
                                        "src/modules/shared/account/accountController.js",
                                        "src/modules/shared/account/accountService.js"
                                    ]
                                }
                            ]).then(function() {
                                var AccountService = $injector.get('AccountService');
                                return AccountService.getUser()

                            });
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/applicant/account', {
                    templateUrl     : 'src/modules/shared/account/accountView.html',
                    controller      : 'AccountController',
                    controllerAs    : 'vm',
                    module          : 'applicant',
                    resolve         : {
                        AccountController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.shared",
                                    files: [
                                        "src/modules/shared/account/accountController.js",
                                        "src/modules/shared/account/accountService.js"
                                    ]
                                }
                            ]).then(function() {
                                var AccountService = $injector.get('AccountService');
                                return AccountService.getUser()

                            });
                        }],
                        checkAuth   : authenticate
                    }
                });

        }
    ]);

})();