;(function() {

    'use strict';

    angular
        .module('module.candidate')
        .controller('ApplicationsController', [ 
            'LxNotificationService'
        ,   'Upload'
        ,   '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   'Sidebar'
        ,   'LxDialogService'
        ,   'ApplicationsService'
        ,   ResumeController
        ]);

        function ResumeController (
            LxNotificationService
        ,   Upload
        ,   $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   Sidebar
        ,   LxDialogService
        ,   ApplicationsService
        ) {

            var vm = this;

            vm.Sidebar = Sidebar;

            ApplicationsService.getApplications().then(function() {
                vm.applications = ApplicationsService.applications;
            });

        }
    }
)();