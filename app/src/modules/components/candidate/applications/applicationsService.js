;(function() {

    'use strict';

    angular
        .module('module.candidate')
        .service('ApplicationsService', ['$resource', '$rootScope', '$window', ApplicationsService]);

        function ApplicationsService ($resource, $rootScope, $window) {

                var api = $resource ('',
                    {},
                    {
                        getApplications : {
                            url     : $rootScope.config.application_server +  'api/candidate/apply/:id',
                            params  : {id : '@id'}, 
                            method  : 'GET'
                        },
                    }
                );

                var ApplicationsService = {
                        
                    applications : null,

                    getApplications : function() {

                        var self = this;
                        return api
                            .getApplications({id : $window.localStorage.user})
                            .$promise
                            .then(function (response) {
                                self.applications = response.applications;
                            })

                    }
                };

                return ApplicationsService;
        }

})();