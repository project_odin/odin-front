;(function() {

    'use strict';

    angular
        .module('module.candidate')
        .controller('ResumeController', [ 
            'LxNotificationService'
        ,   'Upload'
        ,   '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   'Sidebar'
        ,   'LxDialogService'
        ,   'ResumeService'
        ,   ResumeController
        ]);

        function ResumeController (
            LxNotificationService
        ,   Upload
        ,   $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   Sidebar
        ,   LxDialogService
        ,   ResumeService
        ) {

            var vm = this;

            function showAddResumeDialog () {
                LxDialogService.open('addResumeDialog');
            }

            function addResume () {

                Upload.upload({
                    url     : $rootScope.config.application_server +  'api/candidate/resume/upload',
                    fields  : 
                    {
                        'user'      : $window.localStorage.user,
                        'title'     : vm.resume.title,
                        'isDefault' : (vm.resume.isDefault !== null) ? 'y' : 'n'
                    },
                    file    : vm.file
                }).progress(function (evt) {

                    var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                    
                }).success(function (data, status, headers, config) {
                    
                    vm.resume.title = null;
                    vm.resume.isDefault = null;
                    vm.resume.file = null;
                    LxNotificationService.success(data.success);
                    LxDialogService.close('addResumeDialog');

                    ResumeService
                        .getResumeList()
                        .then(function() {
                            vm.resumes = ResumeService.resumes;
                        })


                }).error(function (data, status, headers, config) {
                    
                    LxNotificationService.error(data.error);

                });
            }

            function setDefault (id) {
                ResumeService.setAsDefault(id).then(function() {
                    vm.resumes = ResumeService.resumes;
                })
            }

            vm.Sidebar = Sidebar;
            vm.resume = {
                title     : null,
                isDefault : null,
                file      : null
            };
            
            ResumeService.getResumeList().then(function() {
                vm.resumes             = ResumeService.resumes;
            });

            vm.showAddResumeDialog = showAddResumeDialog;
            vm.addResume           = addResume;
            vm.setDefault          = setDefault;
        }

    }
)();