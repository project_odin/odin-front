;(function() {

    'use strict';

    angular
        .module('module.candidate')
        .service('ResumeService', ['$resource', '$rootScope', '$window', ResumeService]);

        function ResumeService ($resource, $rootScope, $window) {

                var api = $resource ('',
                    {},
                    {
                        uploadResume : {
                            url     : $rootScope.config.application_server +  'api/candidate/resume',
                            method  : 'POST',
                            headers : {'Content-Type':'multipart/form-data; boundary=----WebKitFormBoundary7FFkW4ALJ71zxLST'} 
                        },

                        getResumeList : {
                            url     : $rootScope.config.application_server +  'api/candidate/resume/:id',
                            method  : 'GET',
                            params  : {id : '@id'}
                        },

                        setAsDefault : {
                            url     : $rootScope.config.application_server +  'api/candidate/resume/default/:user/:id',
                            method  : 'PUT',
                            params  : {
                                user : '@user',
                                id : '@id'
                            }
                        }
                    }
                );


                var ResumeService = {
                    
                    resumes : null,

                    uploadResume : function (file) {
                        return api.uploadResume({file : file}).$promise
                    },

                    getResumeList : function () {
                        var self = this;
                        return api.getResumeList({'id' : $window.localStorage.user}).$promise.then(function(response) {
                            self.resumes = response.resumes;
                        })
                    },

                    setAsDefault : function (id) {
                        var self = this;
                        return api.setAsDefault({'id' : id, 'user' : $window.localStorage.user}).$promise
                            .then(function(response) {
                                return self.getResumeList();
                            });
                    }
                };

                return ResumeService;
        }

})();