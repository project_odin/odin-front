;(function() {

    'use strict';

    angular
        .module('module.candidate')
        .service('RegisterService', ['$resource', '$rootScope', RegisterService]);

        function RegisterService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {
                        register : {
                            url     : $rootScope.config.application_server +  'api/candidate/register',
                            method  : 'POST'
                        }
                    }
                );


                var RegisterService = {
                    register : function (params) {
                        return api.register(params).$promise
                    }
                };

                return RegisterService;
        }

})();