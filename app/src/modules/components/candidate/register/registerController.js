;(function() {

    'use strict';

    angular
        .module('module.hr')
        .controller('RegisterController', 
            [
                '$auth', '$http', '$location', '$window', '$rootScope', 
                'Sidebar', 'RegisterService', 'LxNotificationService', RegisterController
            ]
        );

    function RegisterController(
        $auth, $http, $location, $window, $rootScope, 
        Sidebar, RegisterService, LxNotificationService
    ) {

        var vm = this;
        vm.register = register;
        
        function register () {
            
            RegisterService.register(vm.candidate).then(
                function(response) {
                
                    LxNotificationService.success('Registration success, please login');
                    $location.path('login');
                
                }, function(error) {

                    var errors = error.data

                    angular.forEach(errors, function(value, key) {
                        LxNotificationService.error(value);
                    });
            });

        }
    }

})();