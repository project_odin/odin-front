;(function() {

    'use strict';

    angular
        .module('module.candidate')
        .controller('CandidateDashboardController', [
            '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   'Sidebar'
        ,   'DashboardService'
        ,   'LxDialogService'
        ,   'LxNotificationService'
        ,   CandidateDashboardController
        ]);

        function CandidateDashboardController(
            $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   Sidebar
        ,   DashboardService
        ,   LxDialogService
        ,   LxNotificationService
        ) {

            var vm = this;
            
            function viewOpening (openingId) {
                
                DashboardService
                    .getOpening(openingId)
                    .then(function() {
                        vm.opening = DashboardService.opening;
                        LxDialogService.open('viewOpeningDialog');
                    }
                )
            }

            function apply(openingId) {

                var params = {
                    applicant : $window.localStorage.user,
                    opening   : openingId
                }

                DashboardService.submitApplication(params).then(function(response) {

                     LxDialogService.close('viewOpeningDialog');
                     LxNotificationService
                        .confirm(
                            'Application Success', 
                            'You have successfully applied for this opening. Do you want to go to your applicantions?', 
                            { cancel :'Nope', ok :'Yes' }
                            , function(answer)
                            {
                                if (answer) {
                                    $location.path('/applicant/applications');
                                }
                            }
                        );

                }, function (error) {

                    var error_code = error.data.code;

                    if (error_code === 'application_exists') {
                        LxNotificationService.error("You already applied for this position.");
                    } else {

                        LxNotificationService
                            .confirm(
                                'No current resume', 
                                'You dont have any resume uploaded. Do you want to upload one to continue application', 
                                { cancel :'Nope', ok :'Yes' }
                                , function(answer)
                                {
                                    if (answer) {
                                        LxDialogService.close('viewOpeningDialog');
                                        $location.path('/applicant/resume');
                                    }
                                }
                            );
                    }
                });

            }

            DashboardService.getLatestOpening().then(function() {
                vm.latestOpenings  = DashboardService.latestOpenings;
            });

            DashboardService.getTopOpenings().then(function() {
                vm.topOpenings     = DashboardService.topOpenings;
            });

            vm.Sidebar         = Sidebar;
            vm.viewOpening     = viewOpening;
            vm.apply           = apply;
        }

})();