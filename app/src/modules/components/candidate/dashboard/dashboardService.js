;(function() {

    'use strict';

    angular
        .module('module.candidate')
        .service('DashboardService', ['$resource', '$rootScope', DashboardService]);

        function DashboardService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {
                        getOpenings : {
                            url : $rootScope.config.application_server + 'api/candidate/openings/:type',
                            params : {'type' : '@type'},
                            method: 'GET',
                        },

                        getOpening : {
                            url : $rootScope.config.application_server + 'api/candidate/application/:id',
                            params : {'id' : '@id'},
                            method: 'GET',
                        },

                        submitApplication : {
                            url : $rootScope.config.application_server + 'api/candidate/application',
                            method: 'POST',
                        }
                    }
                );


                var DashboardService = {

                    topOpenings : null,
                    latestOpenings : null,
                    opening : null,

                    getLatestOpening : function () {
                        var self = this;
                        return api.getOpenings({'type' : 'latest'}).$promise.then(function(response) {
                            self.latestOpenings = response.openings;
                        });
                    },

                    getTopOpenings : function () {
                        var self = this;
                        return api.getOpenings({'type' : 'top'}).$promise.then(function(response) {
                            self.topOpenings = response.openings;
                        });
                    },

                    getOpening : function(id) {
                        var self = this;
                        return api.getOpening({'id' : id}).$promise.then(function(response) {
                            self.opening = response.opening;
                        })
                    },

                    submitApplication : function(params) {
                        return api.submitApplication(params).$promise;
                    }
                };

                return DashboardService;
        }

})();