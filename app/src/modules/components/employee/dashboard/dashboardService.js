;(function() {

    'use strict';

    angular
        .module('module.employee')
        .service('DashboardService', ['$resource', '$rootScope', DashboardService]);

        function DashboardService ($resource, $rootScope) {

                var api = $resource ('',
                    {},
                    {
                        getAnnouncements : {
                            url : $rootScope.config.application_server + 'api/hr/announcement',
                            method: 'GET',
                        },

                        getAnnouncement : {
                            url : $rootScope.config.application_server + 'api/hr/announcement/:id',
                            method: 'GET',
                            params : {'id' : '@id'}
                        },

                        updateAnnouncement : {
                            url : $rootScope.config.application_server + 'api/hr/announcement/:id',
                            method: 'PUT',
                            params : {'id' : '@id'}
                        },

                        publishAnnouncement : {
                            url : $rootScope.config.application_server + 'api/hr/publish/announcement/:id',
                            method: 'GET',
                            params : {'id' : '@id'}
                        },

                        unpublishAnnouncement : {
                            url : $rootScope.config.application_server + 'api/hr/unpublish/announcement/:id',
                            method: 'GET',
                            params : {'id' : '@id'}
                        },

                        createAnnouncement : {
                            url : $rootScope.config.application_server + 'api/hr/announcement',
                            method: 'POST'
                        }
                    }
                );

                var DashboardService = {
                    
                    announcements : null,
                    announcement : null,

                    createAnnouncement : function(data) {
                        return api.createAnnouncement(data).$promise;
                    },  

                    getAnnouncements : function() {
                        var self = this;

                        return api.getAnnouncements().$promise.then(function(response) {
                            self.announcements = response.announcements
                        })

                    },
                    getAnnouncement : function(id) {
                            
                        var self = this;

                        return api.getAnnouncement({'id' : id}).$promise.then(function(response){
                            console.log(response);
                            self.announcement = response.announcement;
                        })
                    },

                    updateAnnouncement : function(params, id) {

                        var data = {
                            id : id,
                            title : params.title,
                            details : params.details,
                        };

                        return api.updateAnnouncement(data).$promise;

                    },

                    publishAnnouncement : function(id) {
                        return api.publishAnnouncement({'id' : id}).$promise;
                    },

                    unpublishAnnouncement : function(id) {
                        return api.unpublishAnnouncement({'id' : id}).$promise;
                    },

                };

                return DashboardService;
        }

})();