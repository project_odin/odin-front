;(function() {

	'use strict';

	angular
		.module('module.employee')
		.controller('DashboardController', ['$auth', '$http', '$location', '$window', '$rootScope', 'Sidebar', '$filter', 
			'DashboardService', DashboardController]);

		function DashboardController($auth, $http, $location, $window, $rootScope, Sidebar, $filter, DashboardService) {

			var vm = this;
			
			vm.Sidebar = Sidebar;

			vm.announcements = $filter('filter')(DashboardService.announcements, {status : 'p'});

			console.log(vm.announcements);

		}

})();