;(function() {

    'use strict';

    angular
        .module('module.employee.dashboard', ['ngRoute', 'satellizer', 'module.shared'])
        .config(['$routeProvider', '$authProvider', function($routeProvider, $authProvider) {

            var authenticate = ['AuthenticationService', function(AuthenticationService) {
                return AuthenticationService.checkAuth();
            }];


           $routeProvider
                .when('/employee/dashboard', {
                    templateUrl     : 'src/modules/components/employee/dashboard/dashboardView.html',
                    controller      : 'DashboardController',
                    controllerAs    : 'vm',
                    resolve         : {
                        checkAuth   : authenticate
                    }
                });

        }
    ]);

})();