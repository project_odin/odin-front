;(function() {

    'use strict';

    angular
        .module('module.employee')
        .service('LeaveRequestsService', ['$resource', '$rootScope', '$window', LeaveRequestsService]);

        function LeaveRequestsService ($resource, $rootScope, $window) {

                var api = $resource ('',
                    {},
                    {
                        getLeaveRequests : {
                            url : $rootScope.config.application_server + 'api/employee/leave',
                            method: 'GET',
                        },

                        getLeaveRequest : {
                            url : $rootScope.config.application_server + 'api/employee/leave/list/:id',
                            method: 'GET',
                            params : {id : '@id'}
                        },
                        createLeaveRequest : {
                            url : $rootScope.config.application_server + 'api/employee/leave',
                            method : 'POST'
                        },

                        deleteLeaveRequest : {
                            url : $rootScope.config.application_server + 'api/employee/leave/:id',
                            method : 'DELETE',
                            params : {'id' : '@id'}
                        },

                        getRequest : {
                            url : $rootScope.config.application_server + 'api/employee/leave/:id',
                            method: 'GET',
                            params : {id : '@id'}
                        },

                        editRequest : {
                            url : $rootScope.config.application_server + 'api/employee/leave/:id',
                            method: 'PUT',
                            params : {id : '@id'}
                        }
                    }
                );

                var LeaveRequestsService = {

                    leaveRequests : null,
                    leaveRequest : null,
                    request : null, 

                    getLeaveRequests : function(){
                        var self = this;
                        return api.getLeaveRequests().$promise.then(function(response) {
                            self.leaveRequests = response.leaves;
                        })
                    },

                    getLeaveRequest : function(id){
                        var self = this;
                        return api.getLeaveRequest({'id' : $window.localStorage.user}).$promise.then(function(response) {
                            self.leaveRequest = response.leaves;
                        })
                    },

                    createLeaveRequest : function(params){
                        return api.createLeaveRequest(params).$promise;
                    },

                    deleteLeaveRequest : function(id){
                        return api.deleteLeaveRequest({'id' : id}).$promise;
                    },

                    getRequest : function(id){
                        
                        var self = this;

                        return api.getRequest({'id' : id}).$promise.then(function(response){
                            self.request = response.leave;
                        })
                    },

                    editRequest : function(data) {
                        var data = {
                            id : data.id,
                            user_id : data.user_id,
                            date_start : data.date_start,
                            date_end : data.date_end,
                            reason : data.reason,
                            employee_remarks : data.employee_remarks
                        }

                        return api.editRequest(data).$promise;
                    }

                };

                return LeaveRequestsService;
        }

})();