;(function() {

    'use strict';

    angular
        .module('module.employee')
        .controller('LeaveRequestsController', [
            '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   '$timeout'
        ,   'LeaveRequestsService'
        ,   'LxDialogService'
        ,   'LxNotificationService'
        ,   'Filters'
        ,   LeaveRequestsController]);

        function LeaveRequestsController(
            $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   $timeout
        ,   LeaveRequestsService
        ,   LxDialogService
        ,   LxNotificationService
        ,   Filters
        ) {

            var vm = this;
            
            vm.display = 'list';

            vm.toggleList = function(table, callback) {
                vm.display = '';

                vm.leave = null;
                vm.selected_leave_type = null;

                $timeout(function() {
                    vm.display = table; 

                    if (callback) {
                        callback();
                    }

                }, 500);
            }

            vm.filters = [];
            vm.leave   = {};
            vm.loading = false;
            vm.isEdit = false;

            LeaveRequestsService.getLeaveRequest().then(function() {
                vm.leaveRequests = LeaveRequestsService.leaveRequest;
            })

            Filters.getFilters({'filters[]' : ['leave_types']}).then(function(response) {
                vm.filters.leave_types = response.filters.leave_types;
            });

            vm.openDialog = function(dialogId) {
                LxDialogService.open(dialogId);
            }

            vm.editRequest = function(id) {
                LeaveRequestsService.getRequest(id).then(function() {
                    vm.toggleList('create', function(){
                        vm.isEdit = true;
                    });
                    vm.leave = LeaveRequestsService.request;
                    vm.selected_leave_type = { id : vm.leave.leave_type_id, leave_type : vm.leave.leave_information.leave_type};
                })
            }

            vm.editLeave = function() {
                vm.leave.leave_type_id = (vm.selected_leave_type) ? vm.selected_leave_type.id : null;
                vm.leave.user_id = $window.localStorage.user;
                LeaveRequestsService.editRequest(vm.leave).then(function(response){
                    console.log(response);
                })
            }

            vm.saveLeave = function() {

                vm.loading = true;

                var now = Date.parse(Date());
                var startdate = Date.parse(vm.leave.date_start);
                var enddate = Date.parse(vm.leave.date_end);

                if ((startdate >= now) || (enddate >= now)) {
                    
                    if (startdate < enddate) {

                        vm.leave.leave_type_id = (vm.selected_leave_type) ? vm.selected_leave_type.id : null;
                        vm.leave.user_id = $window.localStorage.user;

                        LeaveRequestsService.createLeaveRequest(vm.leave).then(function() {
                            LeaveRequestsService.getLeaveRequests().then(function(response){
                                vm.leaveRequests = LeaveRequestsService.leaveRequests;
                                vm.loading = false;
                            });
                        }, function(error) {

                            var errors = error.data
                            angular.forEach(errors, function(value, key) {
                                LxNotificationService.error(value);
                            });

                            vm.loading = false;

                        });

                    } else {
                        LxNotificationService.error('Invalid dates, please make sure that start date is earlier than end date.');
                        vm.loading = false;
                    }

                } else {
                    
                    LxNotificationService.error('Invalid dates, please make sure that dates are not less than the current date');
                    vm.loading = false;
                }
            }
    }
})();