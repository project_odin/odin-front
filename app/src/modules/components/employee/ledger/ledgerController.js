;(function() {

    'use strict';

    angular
        .module('module.employee')
        .controller('LedgerController', [
            '$auth'
        ,   '$http'
        ,   '$location'
        ,   '$window'
        ,   '$rootScope'
        ,   'Sidebar'
        ,   'LedgerService'
        ,   LedgerController]);

        function LedgerController(
            $auth
        ,   $http
        ,   $location
        ,   $window
        ,   $rootScope
        ,   Sidebar
        ,   LedgerService) {

            var vm = this;
            
            vm.Sidebar = Sidebar;

            LedgerService.getEmployeePayroll().then(function() {
                vm.payroll = LedgerService.payroll;                
            });


        }

})();