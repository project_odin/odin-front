;(function() {

    'use strict';

    angular
        .module('module.employee')
        .service('LedgerService', ['$resource', '$rootScope', '$window', LedgerService]);

        function LedgerService ($resource, $rootScope, $window) {

                var api = $resource ('',
                    {},
                    {
                        getEmployeePayroll : {
                            url : $rootScope.config.application_server + 'employee/ledger',
                            method: 'GET',
                        },
                    }
                );

                var LedgerService = {
                    payroll : null,
                    getEmployeePayroll : function() {
                        var self = this;
                        return api.getEmployeePayroll().$promise.then(function(response) {
                            self.payroll = response.payroll;
                        })
                    }
                };

                return LedgerService;
        }

})();