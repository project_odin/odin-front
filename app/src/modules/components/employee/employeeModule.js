;(function() {

    'use strict';

    angular
        .module('module.employee', ['ngRoute', 'satellizer', 'module.shared'])
        .config(['$routeProvider', '$authProvider', function($routeProvider, $authProvider) {

            var authenticate = ['AuthenticationService', function(AuthenticationService) {
                return AuthenticationService.checkAuth();
            }];


           $routeProvider
                .when('/employee/dashboard', {
                    templateUrl     : 'src/modules/components/employee/dashboard/dashboardView.html',
                    controller      : 'DashboardController',
                    controllerAs    : 'vm',
                    module          : 'employee',
                    resolve         : {
                        DashboardController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.employee",
                                    files: [
                                        "src/modules/components/employee/dashboard/dashboardController.js",
                                        "src/modules/components/employee/dashboard/dashboardService.js"
                                    ]
                                }
                            ]).then(function() {
                                var dashboardService = $injector.get('DashboardService');
                                return dashboardService.getAnnouncements();
                            });
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/employee/leaves', {
                    templateUrl     : 'src/modules/components/employee/leave/leaveRequestsView.html',
                    controller      : 'LeaveRequestsController',
                    controllerAs    : 'vm',
                    module          : 'employee',
                    resolve         : {
                        LeaveRequestsController: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.employee",
                                    files: [
                                        "src/modules/components/employee/leave/leaveRequestsController.js",
                                        "src/modules/components/employee/leave/leaveRequestsService.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/employee/ledger', {
                    templateUrl     : 'src/modules/components/employee/ledger/ledgerView.html',
                    controller      : 'LedgerController',
                    controllerAs    : 'vm',
                    module          : 'employee',
                    resolve         : {
                        LedgerController: ['$ocLazyLoad', function($ocLazyLoad) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.employee",
                                    files: [
                                        "src/modules/components/employee/ledger/ledgerController.js",
                                        "src/modules/components/employee/ledger/ledgerService.js"
                                    ]
                                }
                            ]);
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/employee/account', {
                    templateUrl     : 'src/modules/shared/account/accountView.html',
                    controller      : 'AccountController',
                    controllerAs    : 'vm',
                    module          : 'employee',
                    resolve         : {
                        AccountController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.shared",
                                    files: [
                                        "src/modules/shared/account/accountController.js",
                                        "src/modules/shared/account/accountService.js"
                                    ]
                                }
                            ]).then(function() {
                                var AccountService = $injector.get('AccountService');
                                return AccountService.getUser()

                            });
                        }],
                        checkAuth   : authenticate
                    }
                })
                .when('/employee/profile', {
                    templateUrl     : 'src/modules/components/employee/profile/profileView.html',
                    controller      : 'ProfileController',
                    controllerAs    : 'vm',
                    module          : 'employee',
                    resolve         : {
                        ProfileController: ['$ocLazyLoad', '$injector', function($ocLazyLoad, $injector) {
                            return $ocLazyLoad.load([
                                {
                                    name: "module.employee",
                                    files: [
                                        "src/modules/components/employee/profile/profileController.js",
                                        "src/modules/components/employee/profile/profileService.js"
                                    ]
                                }
                            ]).then(function() {
                                var ProfileService = $injector.get('ProfileService');
                                return ProfileService.getProfile()

                            });
                        }],
                        checkAuth   : authenticate
                    }
                });

        }
    ]);

})();