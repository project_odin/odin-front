;(function() {

    'use strict';

    angular
        .module('module.employee')
        .service('ProfileService', ['$resource', '$rootScope', '$window', ProfileService]);

        function ProfileService ($resource, $rootScope, $window) {

                var api = $resource ('',
                    {},
                    {
                        getProfile : {
                            url : $rootScope.config.application_server + 'api/employee/employee/:id',
                            params : {id : '@id'},
                            method: 'GET'
                        },

                        updateProfile : {
                            url : $rootScope.config.application_server + 'api/employee/employee/:id',
                            params : {id : '@id'},
                            method: 'PUT'                            
                        }
                    }
                );

                var ProfileService = {
                    employeeProfile : null,
                    getProfile : function() {
                        var self = this;
                        var id = $window.localStorage.user;
                        return api.getProfile({'id' : id}).$promise.then(function(response) {
                            self.employeeProfile = response.employee;
                        })

                    },

                    updateProfile : function(data) {
                        return api.updateProfile(data).$promise;
                    }

                };

                return ProfileService;
        }

})();