;(function() {

    'use strict';

    angular
        .module('module.employee')
        .controller('ProfileController', [
            '$auth', 
            '$http', 
            '$location', 
            '$window', 
            '$rootScope', 
            'ProfileService',
            'LxNotificationService', 
            ProfileController]
        );

        function ProfileController(
            $auth, 
            $http, 
            $location, 
            $window, 
            $rootScope, 
            ProfileService,
            LxNotificationService
        ) {

            var vm = this;

            vm.filters = {
                civil_status : [
                    {id : 's', status : 'Single'},
                    {id : 'm', status : 'Married'},
                    {id : 'w', status : 'Widowed'}
                ],
                gender : [
                    {id : 'm', gender : 'Male'},
                    {id : 'f', gender : 'Female'}
                ]
            }

            var profile = ProfileService.employeeProfile;
            vm.employee = {};

            var civil_status = (profile.employees.civil_status === 's') ? 'Single' : 'Married'; 
            var gender = (profile.employees.gender === 'm') ? 'Male' : 'Female'; 

            vm.selected_gender = {id : profile.employees.gender, gender : gender};
            vm.selected_status = {id : profile.employees.civil_status, status : civil_status};

            var date = new Date();

            var year  = date.getFullYear();
            var month = date.getMonth();
            var day   = date.getDate();
            var bdate = year + '-' + month + '-' + day;

            vm.employee.id                 = profile.employees.id;
            vm.employee.employee_number    = profile.employees.employee_number;
            vm.employee.contact_number     = profile.employees.contact_number;
            vm.employee.address            = profile.employees.address;
            vm.employee.birth_date         = (profile.employees.birth_date === '0000-00-00') ? bdate: profile.employees.birth_date;

            vm.employee.account_number     = profile.employees.account_number;
            vm.employee.phil_health_number = profile.employees.phil_health_number;
            vm.employee.sss_number         = profile.employees.sss_number;
            vm.employee.tin_number         = profile.employees.tin_number;
            vm.employee.pagibig_number     = profile.employees.pagibig_number;

            vm.employee.position   = profile.employees.positions.position;
            vm.employee.department = profile.employees.departments.department;
            vm.employee.exemption  = profile.employees.exemptions.description;

            vm.employee.hire_date = profile.employees.hire_date;
            vm.employee.endo_date = profile.employees.endo_date;

            vm.updateProfile = function(){
                console.log(vm.employee);

                var data = {
                    id             : vm.employee.id,
                    gender         : vm.selected_gender.id,
                    civil_status   : vm.selected_status.id,
                    contact_number : vm.employee.contact_number,
                    address        : vm.employee.address,
                    birth_date      : vm.employee.birth_date
                }

                ProfileService.updateProfile(data).then(function() {
                    LxNotificationService.success("Profile updated");
                })

            }
        }

})();