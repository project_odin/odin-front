;(function() {

    'use strict';


    angular
        .module('module.shared')
        .controller('AccountController', [
            '$auth', 
            '$http', 
            '$location', 
            '$window', 
            '$rootScope', 
            'AccountService', 
            'LxNotificationService',
            AccountController]);

        function AccountController(
            $auth, 
            $http, 
            $location, 
            $window, 
            $rootScope, 
            AccountService,
            LxNotificationService
        ) {
            var vm = this;
            vm.user = AccountService.userdata;

            vm.saveProfile = function(){

                LxNotificationService
                    .confirm(
                        'Update Profile', 
                        'Do you want to procceed to update?', { cancel:'No', ok:'Yes' }, function(answer)
                        {
                            if (answer) {
                                var data = {
                                    'id'          : vm.user.profiles.id,
                                    'first_name'  : vm.user.profiles.first_name,
                                    'middle_name' : vm.user.profiles.middle_name,
                                    'last_name'   : vm.user.profiles.last_name
                                }

                                AccountService.saveProfile(data).then(function(response){
                                    LxNotificationService.success(response.message);
                                }, function(error) {
                                     var errors = error.data
                                    angular.forEach(errors, function(value, key) {
                                        LxNotificationService.error(value);
                                    });
                                });
                        }
                    });
            }

            vm.saveCredentials = function(){
                LxNotificationService
                    .confirm(
                        'Update Profile', 
                        'Do you want to procceed to update?', { cancel:'No', ok:'Yes' }, function(answer)
                        {
                            if (answer) {
                                var data = {
                                    id       : vm.user.id,
                                    username : vm.user.username,
                                    password : vm.user.password
                                }

                                AccountService.saveCredentials(data).then(function(response) {
                                     LxNotificationService.success(response.message);
                                }, function(error){
                                     var errors = error.data
                                    angular.forEach(errors, function(value, key) {
                                        LxNotificationService.error(value);
                                    });
                                });
                        }
                    });
            }

        }

})();