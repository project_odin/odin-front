;(function() {

    'use strict';

    angular
        .module('module.shared')
        .service('AccountService', ['$resource', '$rootScope', '$window', AccountService]);

        function AccountService ($resource, $rootScope, $window) {

                var api = $resource ('',
                    {},
                    {
                        getUser : {
                            url : $rootScope.config.application_server + 'api/account/:id',
                            params : {id : '@id'},
                            method: 'GET',
                        },
                        
                        saveProfile : {
                            url : $rootScope.config.application_server + 'api/profile/:id',
                            params : {id : '@id'},
                            method: 'PUT',
                        },

                        saveCredentials : {
                            url : $rootScope.config.application_server + 'api/user/:id',
                            params : {id : '@id'},
                            method: 'PUT',
                        },

                    }
                );

                var AccountService = {
                    userdata : null,
                    getUser : function() {
                        var self = this;
                        var id = $window.localStorage.user;
                        return api.getUser({'id' : id}).$promise.then(function(response) {
                            self.userdata = response.user;
                        })

                    },

                    saveProfile : function(data){
                        return api.saveProfile(data).$promise;
                    },

                    saveCredentials : function(data){
                        return api.saveCredentials(data).$promise;
                    },

                };

                return AccountService;
        }

})();