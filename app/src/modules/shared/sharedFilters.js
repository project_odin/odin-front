;(function() {

    'use strict';

    angular
        .module('module.shared')
        .filter("asDate", function () {
    		return function (input) {
        		return new Date(input);
    		}
		})
        .filter('dateDiff', function () {
            var magicNumber = (1000 * 60 * 60 * 24);

            return function (toDate, fromDate) {
                if(toDate && fromDate){
                    var dayDiff = Math.floor((toDate - fromDate) / magicNumber);
                    if (angular.isNumber(dayDiff)){
                        return dayDiff + 1;
                    }
                }
            };
        })
})();