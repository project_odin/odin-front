// use strict mode to enforce standards
'use strict';

// module configuration for grunt
module.exports = function(grunt) {
    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // mapping of hashed files
    grunt.cacheMap = [];

    // initialize configuration for grunt
    grunt.initConfig({

        // read package.json file of the module
        pkg: grunt.file.readJSON('package.json'),

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        'dist/*',
                    ]
                }]
            }
        },

        /**
         * Reads HTML for usemin blocks to enable smart builds that automatically
         * concat, minify and revision files. Creates configurations in memory so
         * additional tasks can operate on them
         */
        useminPrepare: {
            html: 'app/index.html',
            options: {
                
                dest: 'dist' //Base directory where the transformed files should be placed.
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: 'app',
                    dest: 'dist',
                    src: [
                        '*.html',
                        'vendors/**',
                        'assets/**',
                        'config/**',
                        'libraries/**',
                        'layouts/**',
                        'src/modules/*/**/**/*'
                    ]
                }]
            },
        },
        uglify: {
            all: {
                files: [{
                    expand: true,
                    cwd: 'dist/src',
                    src: [
                        '**/*.js', 
                        '!modules/*/spec/**/*.js',
                        '!common/spec/**/*.js'
                    ],
                    dest: 'dist/src',
                    //ext: '.min.js'
                }]
            }
        },

        // Performs rewrites based on rev and the useminPrepare configuration
        usemin: {
          html: ['dist/{,*/}*.html'],
          css: ['dist/styles/{,*/}*.css'],
          options: {
            assetsDirs: ['dist']
          }
        },  

        // add file revision
        bushcaster: {
            options: {
                hashLength: 8,
                removeSource: true,
                onComplete: function (map, files) {
                    
                    files.forEach(function ( file ) {
                        grunt.cacheMap.push({
                            from: file,
                            to: map[file]
                        });

                    });                    
                }
            },
            main: {
                files: [{
                    expand: true,
                    cwd: 'dist/',
                    src: ['**/*.js', '**/*.css'],
                    dest: './',
                }]
            }
        },
        // replace file references with the revved files
        replace: {
            dist: {
                replacements: grunt.cacheMap,
                overwrite: true,
                src: [
                    'dist/src/**/*.js',
                    'dist/index.html'
                ]
            }
        },
        karma: {
            app: {
                configFile: 'karma.conf.js'
            },
            dist: {
                configFile: 'karma.dist.conf.js'
            }
        }
    });

    grunt.registerTask('test-app', [
        'karma:app'
    ]);

    grunt.registerTask('test-dist', [
        'karma:dist'
    ]);

    // register grunt tasks
    grunt.registerTask('default', [
        // 'test-app',  
        'clean:dist', 
        'useminPrepare', 
        'concat', 
        'copy:dist', 
        'cssmin', 
        'uglify', 
        'usemin', 
        'bushcaster',
        'replace',
        // 'test-dist'
    ]);

};